#!/bin/bash
set -e
# Script to run a local instance of grafana, loki and prometheus
# with a data (prometheus and loki) snapshot of an experiment

function cleanup() {
  docker-compose down -v
  rm -rf ${TMP_BASEDIR}
}

function ctrl_c() {
  echo "Shutting down"
  cleanup
}

function fail() {
  echo -e "$1"
  cleanup
  exit 1
}

docker-compose version &> /dev/null || fail "need docker-compose"

if [[ -z ${1} ]]; then
  echo "Usage: run.sh EXPERIMENT_ARCHIVE || EXPERIMENT_DATA_FOLDER"
  exit 1
fi

TMP_BASEDIR=/tmp/taler-perf
rm -rf ${TMP_BASEDIR} || true
mkdir -p ${TMP_BASEDIR}

export LOKI_DATA="${TMP_BASEDIR}/loki"
export PROMETHEUS_DATA="${TMP_BASEDIR}/prometheus"
export DASHBOARDS_DIR="${TMP_BASEDIR}/dashboards"
mkdir -p ${LOKI_DATA}
mkdir -p ${PROMETHEUS_DATA}
mkdir -p ${DASHBOARDS_DIR}

EXP_DATA=${1}

if file ${EXP_DATA} | grep -q 'tar archive'; then
  # Extract the exp-data dir from the persist archive
  G5K_ARCHIVE=$(tar -t -f ${EXP_DATA} | grep 'g5k')
  tar -xvf ${EXP_DATA} -C /tmp ${G5K_ARCHIVE} 
  tar -xvf /tmp/${G5K_ARCHIVE} -C /tmp exp-data 
  rm -rf /tmp/${G5K_ARCHIVE}
  EXP_DATA=/tmp/exp-data
  mv ${EXP_DATA}/* ${TMP_BASEDIR}
  rm -rf ${EXP_DATA}
else
  cp -r ${EXP_DATA} ${TMP_BASEDIR}
fi

# load the times to adjust the grafana dashboards
source "${TMP_BASEDIR}/times.env" || fail \
"Archive does not contain times.env,
make sure to pass an uncomressed tar archive created with persist.sh or the unpacked exp-data folder"

START=$(date --date=@${EXPERIMENT_START} +"%F %T")
END=$(date --date=@${SNAPSHOT_TIME} +"%F %T")

# to get the right permissions on the Prometheus data
export U_ID=$(id -u)
export G_ID=$(id -g)

function startup() {

  # prepare the dashboards for grafana
  # need to replace the DS_PROMETHEUS/DS_LOKI placeholders which are created when
  # exporting and set the timestamps that it works out of the box without
  # having to search for the experiment time in the browser
  for FILE in $(find ../grafana -iname "*json") 
  do
    OUTPUT="${DASHBOARDS_DIR}/$(basename $FILE)"
    sed -e 's/${DS_PROMETHEUS}/default-prometheus/g' \
	-e 's/${DS_LOKI}/default-loki/g' $FILE | \
    jq --arg f "${START}" \
       --arg t "${END}" \
       '.time.from=$f | .time.to=$t' > $OUTPUT
  done

  docker-compose down
  docker-compose up &
}

trap ctrl_c INT

startup
while ! wget -q -O /dev/null http://localhost:8080; do
  sleep 2 || exit 1
done
echo "Instances running, please head to http://localhost:8080"
xdg-open http://localhost:8080 || true
wait
