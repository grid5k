#!/usr/bin/env python3
# Script to analyze the queries executed against the taler-exchange db

from json import loads
from re import compile, sub, escape
from subprocess import check_output, CalledProcessError, STDOUT
from enum import Enum
from textwrap import fill
import argparse
from datetime import datetime
from os import mkdir, path
import sys

try:
    import sqlparse
    have_sqlparse = True
except ImportError:
    have_sqlparse = False
    print("NOTE: install sqlparse to get prettier sql statements")


class Type(Enum):
    RELATION = 0
    MODIFY = 1
    OTHER = 2


class Plan:
    def __init__(self, name: str, _type: Type, indent: int):
        self.name = name
        self.partitions_hit = 0
        self.would_have_hit_partitions = 0
        self.rows_estimated = 0
        self.rows_returned = 0
        self.time_spent = 0
        self.scan_type = ""
        self.used_index = ""
        self.condition = ""
        self.type = _type
        self.operation = ""
        self.tuples_inserted = 0
        self.tuples_conflicting = 0
        self.indent = " " * indent

    def print(self):
        if self.type == Type.RELATION:
            self._print_relation()
        elif self.type == Type.MODIFY:
            self._print_modify()
        elif self.type == Type.OTHER:
            self._print_other()

    def _print_relation(self):
        print(self.indent + self.name + ":\n")
        if self.partitions_hit == 0:
            print(self.indent + f"    Not touched")
            print(self.indent + f"    Partitions would be Hit: {self.would_have_hit_partitions}")
        else:
            print(self.indent + f"    Partitions Hit: {self.partitions_hit}")
            print(self.indent + f"    Time Spent:     {self.time_spent} ms")
            print(self.indent + f"    Rows Estimated: {self.rows_estimated}")
            print(self.indent + f"    Rows Returned:  {self.rows_returned}")
            print(self.indent + f"    Scan Method:    {self.scan_type}")
        if self.scan_type == 'Index Scan':
            print(self.indent + f"    Index Used: {self.used_index}")
        print("")

    def _print_other(self):
        print(self.indent + f"{self.name}:")
        print("")

    def _print_modify(self):
        print(self.indent + f"{self.name}:\n")
        print(self.indent + f"  Operation:         {self.operation}")
        print(self.indent + f"  Tuples Inserted:   {self.tuples_inserted}")
        if self.tuples_conflicting != 0:
          print(self.indent + f"  Tuples Conflicting: {self.tuples_conflicting}")
        print("")


# All plans from a single analyze will be stored here
plans = {}

# The plan which was the most time consuming 
most_time_consuming = Plan("default", Type.RELATION, 0)

# Regex which strips the partition number from a partition
# Must be either of the suffixes: _<N> or _default
partitioned_re = compile('.*_([0-9]+|default)')

# Regexes which will be compiled at start when the identifier of the
# log lines is set via cli arguments
log_statement_re = None
log_execute_re = None
log_get_params_re = None

# Command line arguments
args = None

# In which directories to store the current analyzation
output_dir = None

# Number of queries analyzed
queries = 0

def print_query(sql):
    """
    Print an sql query to stdout
    """
    print("\n" + "=" * 80 + "\n")
    if have_sqlparse:
        print(sqlparse.format(sql, 
                              reindent=True, 
                              keyword_case='upper', 
                              wrap_after=80))
    else:
        print(fill(text=sql, width=80))
    print("\n" + "=" * 80 + "\n")


def get_explain(sql) -> dict:
    """
    Execute EXPLAIN ANALYZE against the database, returns the plan as JSON dictonary
    """
    sql = sql.replace("\n", " ").replace('"', "")
    sql = "EXPLAIN (ANALYZE, VERBOSE, BUFFERS, FORMAT JSON) " + sql + ";"
    try:
        analyze_json = check_output(
                args=['psql',
                      '-U', args.db_user,
                      '-h', args.db_host,
                      '-d', args.db_name,
                      '-p', args.db_port,
                      '-P', 'pager=off',
                      '-qtAXc', sql],
                env={'PGPASSWD': args.db_pw},
                timeout=10,
                stderr=STDOUT
        )
        return loads(analyze_json)[0]
    except CalledProcessError as e:
        if not args.ignore:
            print(e.output.decode('utf-8'), file=sys.stderr)
            print("\nQuery returned an error, aborting\n", file=sys.stderr)
            exit(1)


def setup_log_regex():
    """
    Compile the regular expressions used to identify statements in the logs
    Must be called only then log_id is configured in args
    """
    global log_statement_re, log_execute_re, log_get_params_re
    log_statement_re = compile(f'.*{args.log_id} LOG:  statement: ((?!COMMIT)(?!BEGIN)(?!SET)(?!START).*);')
    log_execute_re = compile(f'.*{args.log_id} LOG:  execute ([a-zA-Z0-9_]+): ((?!COMMIT)(?!BEGIN)(?!SET)(?!START)[^#]+)(?:#.*DETAIL:  parameters: (.*))?')
    log_get_params_re = compile('(\$\d+)+ = ([^, ]+)')


def ensure_relation_added(relation, indent):
    """
    Ensure that a relation is added in the plans dictionary
    """
    if relation not in plans:
        plans[relation] = Plan(relation, Type.RELATION, indent)

def ensure_plan_added(plan, indent):
    """
    Ensure an uncategorized plan is added in the plans dictionary
    """
    idx = plan + str(indent)
    plans[idx] = Plan(plan, Type.OTHER, indent)


def ensure_modify_added(plan, indent):
    """
    Ensure a plan with type 'ModifyTable' is in the plans dictionary
    """
    idx = plan['Relation Name']
    p = Plan(plan['Relation Name'], Type.MODIFY, indent)
    p.operation = plan['Operation']
    p.time_spent = plan['Actual Total Time']
    if 'Tuples Inserted' in plan:
      p.tuples_inserted = plan['Tuples Inserted']
      p.tuples_conflicting = plan['Conflicting Tuples']
    plans[idx] = p


def check_most_expensive_time(plan: Plan):
    """
    Check and update the most expensive relation scan
    """
    global most_time_consuming
    if most_time_consuming.time_spent < plan.time_spent:
        most_time_consuming = plan


def parse_scan(plan, indent):
    """
    Parse a JSON plan which scans a relation (not CTE Scans)
    """
    relation = plan['Relation Name']

    # Remove partition suffix as we do not want each partition, 
    # but only the relation shown
    if partitioned_re.match(relation):
        relation = relation.split('_')[0:-1]
        relation = "_".join(relation)

    ensure_relation_added(relation, indent)
    _plan = plans[relation]

    if plan['Actual Loops'] > 0:
        _plan.partitions_hit += 1
        _plan.time_spent += (plan['Actual Total Time'] * plan['Actual Loops'])
        _plan.rows_estimated += plan['Plan Rows']
        _plan.rows_returned += plan['Actual Rows']
        check_most_expensive_time(_plan)
    else:
        _plan.would_have_hit_partitions += 1

    if _plan.scan_type == "":
        _plan.scan_type = plan['Node Type']
        if _plan.scan_type == 'Index Scan':
            _plan.used_index = plan['Index Name']


def iterate_plans():
    """
    Iterate the plans dict and return each plan
    """
    for plan in plans:
        yield plans[plan]


def parse_plan(plan, indent=2):
    """
    Parse the JSON plan returned from EXPLAIN (ANALYZE, BUFFERS, VERBOSE FORMAT JSON)
    """
    if plan['Node Type'] == 'Function Scan':
        ensure_plan_added(plan['Node Type'], indent)
    elif 'Scan' in plan['Node Type'] and 'CTE' not in plan['Node Type']:
        parse_scan(plan, indent + 2)
    elif plan['Node Type'] == 'ModifyTable':
        if plan['Operation'] == 'Insert':
            ensure_modify_added(plan, indent)
        elif plan['Operation'] == 'Update':
            for _plan in plan['Plans']:
                parse_plan(_plan, indent+2)
    elif 'Plans' in plan:
        ensure_plan_added(plan['Node Type'], indent)
        for _plan in plan['Plans']:
            parse_plan(_plan, indent + 2)


def print_summary_and_get_total(attr_name) -> int:
    """
    Print a summary of the attribute with attr_name of the Plan class and return the total
    """
    total = 0
    for plan in iterate_plans():
        if plan.type == Type.RELATION:
            attr = getattr(plan, attr_name)
            total += attr
            print("  {:<25} {:>10}".format(plan.name + ":", attr))
    print(f"\n  (Total: {total})")
    return total


def print_and_get_non_indexed() -> int:
    """
    Print all non indexed scans and return the total amount
    """
    total = 0
    for plan in iterate_plans():
        if plan.scan_type == 'Seq Scan':
            print(f"  {plan.name}")
            total += 1
    print(f"\n  (Total: {total})")
    return total


def parse_visualize_and_get_summary(analyze_json) -> str:
    """
    Parse and visualize a json plan returned from EXPLAIN (ANALYZE, BUFFERS, VERBOSE, FORMAT JSON)
    Returns a string with the summary (time, actual rows, sub partition rows, partitions hit, non indexed scans)
    """
    if analyze_json is None:
        return

    plan = analyze_json['Plan']
    if 'JIT' in analyze_json:
        time = analyze_json['JIT']['Timing']['Total']
    else:
        time = analyze_json['Execution Time']

    print("GENERAL:\n")
    print("  {:<25} {:>10}".format("Estimated Rows:", plan['Plan Rows']))
    print("  {:<25} {:>10}".format("Actual Rows Returned:", plan['Actual Rows']))
    print("  {:<25} {:>10} ms".format("Actual Execution Time:", time))

    parse_plan(plan)

    print("\n\nSUMMARY: \n")
    print(f"Rows Returned by Sub-queries:")
    print("-----------------------------\n")
    total_sub_rows = print_summary_and_get_total('rows_returned')
    print("\nPartitions Hit:")
    print("---------------\n")
    total_partitions_hit = print_summary_and_get_total('partitions_hit')
    print("\nPartitions Hit on full Execution (will be hit if filters match):")
    print("---------------\n")
    total_possible_hits = print_summary_and_get_total('would_have_hit_partitions')
    print("\nNon Indexed Scans on:")
    print("---------------------\n")
    total_non_indexed_scans = print_and_get_non_indexed()
    print("\nMost Time Consuming:")
    print("--------------------\n")
    print("  {:<25} {:>10} ms".format(
        most_time_consuming.name + ":", 
        most_time_consuming.time_spent)
    )
    print("")

    if args.verbose:
        print("\nDETAIL:\n")
        for p in iterate_plans():
            p.print()
    print("")

    return "{:<10} {:<10} {:<10} {:<10} {:<10}".format(
      time,
      plan['Actual Rows'],
      total_sub_rows,
      str(total_partitions_hit)+" ("+str(total_possible_hits)+")",
      total_non_indexed_scans
    )


def handle_query():
    """
    Handle and explain analyze a query passed as argument
    """
    explain = get_explain(args.sql)
    print_query(args.sql)
    parse_visualize_and_get_summary(explain)


def analyze_log_query(name, sql):
    """
    Analyze a statement extracted from the logs
    """
    # reset global plans
    global plans, most_time_consuming, queries

    summary = None
    plans = {}
    most_time_consuming = Plan("default", Type.RELATION, 0)

    name = str(queries) + "_" + name

    # save the original stdout that it can be reassigned later
    orig_stdout = sys.stdout

    if explain := get_explain(sql):
        with open(path.join(output_dir, name + ".txt"), "w+") as of:
            # Override default stdout so we can simply print in the functions
            sys.stdout = of
            print_query(sql)
            summary = parse_visualize_and_get_summary(explain)

    sys.stdout = orig_stdout

    if summary is None:
        summary = "error, ignored"

    # Still print, even if failed so we got the same amount each time
    print("{:<40} {}".format(name, summary))
    queries = queries + 1


def handle_log_statement(match):
    """
    Handle a log line which begins with statement:
    """
    statement = match.group(1)
    analyze_log_query("direct", statement)
    

def handle_log_execute(match):
    """
    Handle a log line which begins with execute:
    This method will replace all parameters in the statement which are found in `parameters:'
    """
    params = None
    name = match.group(1)
    statement = match.group(2)
 
    if '$' in statement:
        _parameters = match.group(3)
 
        if not _parameters:
            print_query(statement)
            err = "ERROR: Could not find query parameters\n" \
                  "Make sure to set the following in postgres.conf before collecting logs:\n\n" \
                  "  'log_statement=all'\n" \
                  "  'syslog_split_messages=off'\n" \
                  "  'log_error_verbosity=default'\n"
            print(err, file=sys.stderr)
            exit(1)

        params = log_get_params_re.findall(_parameters)
 
        for param in params:
            statement = sub(escape(param[0]) + r'\b', 
                            escape(param[1]), 
                            statement)

    analyze_log_query(name, statement)


def create_output_dir(basename):
    """
    Create the directory in which all current analyzes will be written to
    """
    global output_dir
    dirname = basename + "-" + str(round(datetime.now().timestamp()))
    output_dir = dirname
    try:
        mkdir(dirname)
    except OSError as e:
        print("ERROR: could not create directory: " + e, file=sys.stderr)
        exit(1)


def handle_logs():
    """
    Parse a log file and explain analyze all statements found matching the log_id.
    The script expects all parameters and the whole statement in one log line.
    Thus the following parameters must be set in postgres.conf:
      log_statement=all
      syslog_split_messages=off
      log_error_verbosity=default
    (Syslog must be able to handle large messages)
    """
    logs = None

    create_output_dir(path.splitext(path.basename(args.file))[0])

    # Needs to be called here since log_id is not set previously
    setup_log_regex()

    try:
        logs = open(args.file, 'r')
    except IOError as e:
        print(f"Error: cannot open file {args.file}")
        print(e.strerror)
        exit(1)

    print("{:40} {:<10} {:<10} {:<10} {:<10} {:<10}".format(
        "name",
        "t (ms)",
        "act-rws",
        "sub-q-rws",
        "prts",
        "n-idx"
    ))

    for line in logs.readlines():

        if match := log_statement_re.match(line):
            handle_log_statement(match)

        elif match := log_execute_re.match(line):
            handle_log_execute(match)

    logs.close()
    print(f"\nINFO: detailed ouptut written to {output_dir}/N_statement_name.txt")


def main():
    parser = argparse.ArgumentParser(description='Summarize Explain Analyze of GNU Taler Database')

    global_args = argparse.ArgumentParser(add_help=False)
    global_args.add_argument('-U', '--user',
                             dest='db_user',
                             type=str,
                             default="",
                             help='Database user, default current user')
    global_args.add_argument('-P', '--password',
                             dest='db_pw',
                             type=str,
                             default="",
                             help='Database password, default ""')
    global_args.add_argument('-H', '--host',
                             dest='db_host',
                             type=str,
                             default="",
                             help='Database host, default unix socket')
    global_args.add_argument('-d', '--database',
                             dest='db_name',
                             type=str,
                             default="",
                             help='Database name, default current user')
    global_args.add_argument('-p', '--port',
                             dest='db_port',
                             type=str,
                             default="",
                             help='Database port, default 5432')
    global_args.add_argument('-v', '--verbose',
                             dest='verbose',
                             action='store_true',
                             help='Print detailed query plan')
    global_args.add_argument('-i', '--ignore-errors',
                             dest='ignore',
                             action='store_true',
                             help='Continue when an SQL error occurs')

    s_p = parser.add_subparsers()

    log_p = s_p.add_parser('logs',  
                           parents=[global_args],
                           help='Execute queries from a log file (postgres must have set log_statement=all).')
    log_p.add_argument('-l', '--log-identifier',
                       dest='log_id',
                       type=str,
                       default="taler@taler-exchange",
                       help='Identifier (user@db_name) to filter which statements from the logs to analyze (taler@taler-exchange)')
    log_p.add_argument(metavar='FILE',
                       dest='file',
                       type=str,
                       help='Log file to parse and execute queries')
    log_p.set_defaults(func=handle_logs)

    sql_p = s_p.add_parser('query',
                           parents=[global_args],
                           help='Execute a specified query')
    sql_p.add_argument(metavar='SQL',
                       dest='sql',
                       type=str,
                       help='The query to run and analyze')
    sql_p.set_defaults(func=handle_query)

    global args
    args = parser.parse_args()
    
    try:
        args.func()
    except:
        parser.print_help()
        parser.exit()


if __name__ == '__main__':
    main()
