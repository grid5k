#!/bin/bash
# Create plots from the grafana dashboard
# $1: [optional] name of the experiment
#
# Additional env: FROM, from when the plots should start
# e.g. export FROM=$(date --date="today 08:30:00" +%s)
# Additional env: TO, OPTIONAL, to when the plots should end
# e.g. export TO=$(date +%s)
set -eu

# Setup the plotter from source
PLOTTER_DIR=plotter

source ../../experiment/.env

if [ ! -d ${PLOTTER_DIR} ]; then
  git clone https://github.com/bossm8/grafana-dashboard-plotter.git ${PLOTTER_DIR}
fi

cd ${PLOTTER_DIR} && git pull 

if [ ! -d venv ]; then
  python3 -m venv venv
fi

source venv/bin/activate
pip install -r requirements.txt > /dev/null

# Apply the configuration for the plotter by using the experiments env config
sed -e "s|\${ADMIN_API_KEY}|${GRAFANA_API_KEY}|g" \
    -e "s|\${BASE_URL}|${GRAFANA_HOST}|g" \
    ../config.yaml > config.yaml

set +u

TO=${TO:-$(date +%s)}

if [ -z "${FROM}" ]; then
  # Start with the configured default range (config.yaml - 4h)
  python3 plots.py --to=${TO}
else
  python3 plots.py --from=${FROM} --to=${TO}
fi

NAME=${1:-${TO}}

# Slugify the name
NAME=$(\
  echo "$NAME" | \
  sed -r s/[~\^]+//g | \
  sed -r s/[^a-zA-Z0-9]+/-/g | \
  sed -r s/^-+\|-+$//g | \
  tr A-Z a-z \
)

# Make sure no existing archive is overridden
N=1
while [ -f ../plots-${NAME}.tar.gz ]; do
  NAME="${NAME}-${N}"
  ((N++))
done

# Create an archive and remove the plots created
tar -czvf ../plots-${NAME}.tar.gz plots

rm -rf plots 
