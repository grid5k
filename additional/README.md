# Additional Resources for Experiments

## persist.sh

Backup Grid'5000 shares which were created by an experiment.
Contains all logs and the node configuration.

Simply use with: 

```bash
./persist.sh -b <BACKUP_NAME> <OPTIONAL_PLOT_ARCHIVE_TO_INCLUDE>`.
```

Once the backup succeeded it is a good idea to delete the remaining data on the NFS:

```bash
./persist.sh -d
```

## plots

### plot.sh

Create png plots from the grafana experiment dashboards using 
[Grafana Dashboard Plotter](https://github.com/bossm8/grafana-dashboard-plotter).

To use this script, the `.env` file located in `experiment` needs to be configured 
since grafana configuration is read from there.

Read config.yaml, plot.sh and the README of the dashboard plotter for help.

Basically the steps are the following (inside plots directory):

* Export the experiment start time as a unix timestamp:
  `export FROM=$(date --date="today 08:30:00" +%s)`
  If not done, now-4h is taken as default (override in config.yaml).
* Optionally export the end of the experiment:
  `export TO=$(date +%s)`
  If not done, now is taken as default.
* Run the script:
  `./plot.sh <OPTIONAL_NAME>`
  `OPTIONAL_NAME` can be a name for the experiment, it is used
  in the archive which is created once the plots are finished.
  Default is `TO`.

## explain-visualizer

A python script which is able to execute and and summarize an SQL query against
an exchange database. It is able to execute statements provided as argument but also
directly from postgres logs. In order for the logs be recognizable the following must
be configured in `postgresql.conf`:

* `log_statement=all`
* `log_error_verbosity=default`
* `syslog_split_messages=off`

Make sure that syslog is capable to receive bigger messages (e.g. `$MaxMessageSize` in rsyslog).

There is also a `docker-compose` which can be used to easily spawn a database automatically applying
the dump from the experiment database. To do so run the following steps (in its directory):

* `export DUMP_FILE=<absolute-or-relative-path-to-dump>`
* `docker-compose up -d`
* `./explain.py logs -U taler -d taler -H localhost <log-file>`

## grafana

Dashboard json files to upload to grafana - also needed for experiment recovery.

### Custom

Contains all *custom* (and downloaded library) dashboards for the experiments. 
Import them vie `Create->Import->Upload JSON` (plus sign) 

The database dashboard is a combination of the following two (plus some custom custom panels):

* [Postgres Overview](https://grafana.com/grafana/dashboards/455): 455 
* [PostgreSQL Statistics](https://grafana.com/grafana/dashboards/6742): 6742

### Library

Additional ones needed can be imported from the library (or as described above).
In your Grafana instance head to `Create->Import->Import via grafana.com` 
and copy those ID one after another:

* [Node Exporter Full](https://grafana.com/grafana/dashboards/1860): 1860
* [Nginx Exporter](https://grafana.com/grafana/dashboards/12708): 12708

## rspecs

Contains various kinds of rpsec files which can be used with jFed. The main files
can be found in `../experiment`.

## recover

Docker setup to recreate the Grafana dashboards of a past experiment. Please refer
to `recover/README.md` for usage information.
