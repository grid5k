# Taler experiments for Grid5000

## Directory Strucutre

In most directories you will find another README
which describes how the parts in there can be used.

### Experiments

Contains all configurations and scripts needed
for the experiment to run in the grid. They are setup to 
work with [jFed](https://jfed.ilabt.imec.be/).

### Image

Contains the Grid5000 Environment configuration. 
This needs to be built and copied to the grid before
any experiments can take place.

### Docker

Some automated way to create the image and directly copy it 
to the grid. 
No need to install the required build tools on your machine.

### Additional

Additional resources such as grafana dashboards and
scripts/configurations which can be used to e.g. persist
data of experiments.

### Configs

Contains the configurations for the applications in the environment.
They will be adjusted copied to '/' once an experiment is started
(make sure that they match the images directory structure).

**NOTE**: Postgres configuration is located in `experiment/script/database[-centos].sh`
          and not in `configs`

## Quick Start

To run an experiment, you need to

* (optionally) have a grafana instance
* Make sure the environment exists in the public direcory which is configured in 
  `experiment/taler.rspec`. If its not in the grid, use `image/README.md` or `docker/README.md`
  to see how to build such an environment.
* Read `experiment/README.md` for instructions on how to run an experiment on Grid'5000. 
  
