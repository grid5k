#==============================================================================
# vim: softtabstop=2 shiftwidth=2 expandtab fenc=utf-8 cc=81 tw=80
#==============================================================================
#
# DESCRIPTION: Recipe extending a Grid'5000 environment recipe supported by
# the technical team. The build will be similar to the one of the supported
# environments, involving puppet notably.
#
#==============================================================================
# This recipe extends another. To look at the step involed, run:
#   kameleon dryrun taler-debian11.yaml
# To see the variables that you can override, use the following command:
#   kameleon info taler-debian11.yaml
---
extend: grid5000/debian11-x64-nfs.yaml

global:
  ### Uncomment and adapt the global variables below as needed

  ## Export format to generate
  # appliance_formats: qcow2 tar.zst

  ## Environment description customization
  ## Author
  g5k_author: "bossm8@bfh.ch"
  ## Version
  g5k_version: 2
  ## Environment image path and compression
  # Note: setup for the docker build image - please replace G5K_HOST and G5K_USER for manual builds
  g5k_tar_path: "http://public.lyon.grid5000.fr/~$${g5k_user}/taler-debian11.tar.zst"
  # g5k_tar_compression: "zstd"
  ## Environment postinstall path, compression, and script command
  # g5k_postinst_path: server:///grid5000/postinstalls/g5k-postinstall.tgz
  # g5k_postinst_compression: "gzip"
  # g5k_postinst_script: g5k-postinstall --net debian
  ## Environment kernel path and params
  # g5k_kernel_path: "/vmlinuz"
  # g5k_initrd_path: "/initrd.img"
  g5k_kernel_params: "mitigations=off"
  ## Environment visibility
  # g5k_visibility: "shared"
  taler_build_packages: "recutils autoconf uncrustify autopoint libtool python3-pip libgcrypt20-dev libjansson-dev libcurl4-gnutls-dev libsodium-dev libidn2-dev libunistring-dev libgnutls28-dev libsqlite3-dev libqrencode-dev valgrind libpq-dev texinfo gdb make zip python3-distutils pkg-config"
  taler_packages: "nginx postgresql-13 postgresql-contrib curl jq bc sudo git zile dnsutils prometheus-postgres-exporter prometheus-nginx-exporter net-tools netcat parallel nodejs tshark dnsmasq bind9 systemd-coredump bash-completion pgbouncer pgstat linux-perf"
  taler_packages_no_recommends: "prometheus prometheus-node-exporter prometheus-pgbouncer-exporter"
  taler_disable_services: "nginx postgresql prometheus prometheus-postgres-exporter prometheus-nginx-exporter prometheus-node-exporter named gettext-base dnsmasq prometheus-pgbouncer-exporter pgbouncer"
  taler_loki_version: "v2.4.2"
  taler_path: /root/taler
  gnunet_commit_sha: master
  exchange_commit_sha: master
  merchant_commit_sha: master
  wallet_commit_sha: master
  grid5k_commit_sha: master
  libmicrohttpd_cflags: "-O2"
  gnunet_cflags: "-O2"
  exchange_cflags: "-O2"
  merchant_cflags: "-O2"
  ## Other parameters can be changed, see kameleon info debian10-taler.yaml
bootstrap:
  ### The bootstrap section takes in charge the initial installation of the
  ## system (distribution installation). No modification should be needed here.
  - "@base"

setup:
  ### The setup section is where customizations of the system take place.
  ## We can request steps from the extended recipe to be executed
  - "@base"
  ## We add steps required by our customization after or before @base. Use
  ## kameleon dryrun debian10_custom.yaml to see the resulting steps in the build.

  - install:
    - packages:
      - exec_in: |
         apt update
         apt upgrade -y
         apt install -y curl
         curl -sL https://deb.nodesource.com/setup_16.x | bash -
         apt install -y $${taler_packages} $${taler_build_packages}
         apt install -y --no-install-recommends $${taler_packages_no_recommends}

         mkdir -p $${taler_path}

    - microhttpd: 
      - exec_in: |
         cd $${taler_path} 

         git clone https://git.gnunet.org/libmicrohttpd.git
         cd libmicrohttpd

         # We need sudo, otherwise we'll get a strange error
         sudo ./bootstrap
         CFLAGS="$${libmicrohttpd_cflags}" ./configure --enable-logging=verbose --prefix=/usr # --enable-sanitizer
         make || true
         make install
         ldconfig

    - gnunet: 
      - exec_in: |
         cd $${taler_path}

         git clone https://git.gnunet.org/gnunet.git
         cd gnunet
         git checkout $${gnunet_commit_sha} || true
         git pull || true

         # We need sudo, otherwise we'll get a strange error
         sudo ./bootstrap
         CFLAGS="$${gnunet_cflags}" ./configure --enable-logging=verbose --prefix=/usr --enable-poisoning=false # --enable-sanitizer
         make || true
         make install
         ldconfig

    - exchange:
      - exec_in: |
         cd $${taler_path}

         pip install jinja2

         git clone https://git.taler.net/exchange.git
         cd exchange
         git checkout $${exchange_commit_sha} || true
         git pull || true

         # We need sudo, otherwise we'll get a strange error
         sudo ./bootstrap
         CFLAGS="$${exchange_cflags}" ./configure --enable-logging=verbose --prefix=/usr # --enable-sanitizer
         make || true
         make install
         ldconfig

    - merchant:
      - exec_in: |
         cd $${taler_path}

         git clone https://git.taler.net/merchant.git
         cd merchant
         git checkout $${merchant_commit_sha} || true
         git pull || true

         # We need sudo, otherwise we'll get a strange error
         sudo ./bootstrap
         CFLAGS="$${merchant_cflags}" ./configure --enable-logging=verbose --prefix=/usr # --enable-sanitizer
         make || true
         make install
         ldconfig

    - wallet: 
      - exec_in: |
         cd $${taler_path}

         git clone https://git.taler.net/wallet-core.git
         cd wallet-core
         git checkout $${wallet_commit_sha} || true
         git pull || true

         npm install -g pnpm

         ./bootstrap
         ./configure --prefix=/usr
         make install

    - grid5k:
      - exec_in: |
         cd $${taler_path}

         git clone git://git.taler.net/grid5k.git 
         cd grid5k
         git checkout $${grid5k_commit_sha} || true

         ./experiment/scripts/createusers.sh

    - loki-promtail:
      - exec_in: |
         apt-get install -y unzip curl

         curl -O -L "https://github.com/grafana/loki/releases/download/$${taler_loki_version}/loki-linux-amd64.zip"
         unzip loki-linux-amd64.zip
         rm loki-linux-amd64.zip
         mv loki-linux-amd64 /usr/local/bin/loki
         chmod 770 /usr/local/bin/loki

         curl -O -L "https://github.com/grafana/loki/releases/download/$${taler_loki_version}/promtail-linux-amd64.zip"
         unzip promtail-linux-amd64.zip
         rm promtail-linux-amd64.zip
         mv promtail-linux-amd64 /usr/local/bin/promtail
         chmod 770 /usr/local/bin/promtail

    - disable_services:
      - exec_in: |
         /etc/init.d/pgbouncer stop
         systemctl daemon-reload
         systemctl stop $${taler_disable_services}
         systemctl disable $${taler_disable_services}

export:
  ### The export section takes in charge the export of your customized Grid'5000
  ## environment. No modification should be needed here.
  - "@base"

