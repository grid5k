# Grid'5000 Environment creation

Official documentation can be found on these links:

* [Grid'5000 Environment](https://www.grid5000.fr/w/Environments_creation_using_Kameleon_and_Puppet)
* [Grid'5000 Kadeploy](https://www.grid5000.fr/w/Advanced_Kadeploy)
* [Kameleon Documentation](http://kameleon.imag.fr/grid5000_tutorial.html)

## Images

There are two variants, debian11 and centos8 please change into the respective directory 
before running any commands. Please do also replace debian11 with the corresponding name.

**NOTE** Centos8 does not have nfs enabled and can only be used to run Postgresql currently.

## Manual Build

Replace `<G5K_USER>` with your Grid'5000 username.
This variable is required, if not specified the build will fail.

```bash
kameleon build -g g5k_user:<G5K_USER> taler-debian11
```

**NOTE** Make sure that all dependencies listed in
[Grid'5000 Environment](https://www.grid5000.fr/w/Environments_creation_using_Kameleon_and_Puppet)
are installed

### Additional Variables

#### Source

The Taler binaries are built from source. You have the possiblity to override the commit from 
which should be built with the following variables (default `master`):

`gnunet_commit_sha`, `exchange_commit_sha`, `merchant_commit_sha`, `wallet_commit_sha` and `grid5k_commit_sha`

(All except `gid5k_commit_sha` are for the debian11 environment only)

#### Build Flags

For each package built from source there are CFLAG variables which can be passed to the image build:

`libmicrohttpd_cflags`, `gnunet_cflags`, `exchange_cflags` and `merchant_cflags`

(debian11 only)

#### Usage

To override them you must pass them with the `-g` option of `kameleon build`:

```bash
kameleon build -g g5k_user:<G5K_USER> gnunet_commit_sha:master libmicrohttpd_cflags:"-O0 -g" taler-debian11
```

For more information please run `kameleon build --help`

### Deploy

Copy the image to a Grid'5000 site:

```bash
cd build/taler-debian11
scp taler-debian11.* <G5K_USER>@access.grid5000.fr:<G5K_SITE>/public/
```

**NOTE** `G5K_USER` and `G5K_SITE` must match the ones in taler-debian11.dsc
`G5K_SITE` defaults to `lyon`.

## Usage

Place `http://public.lyon.grid5000.fr/~<G5K_USER>/taler-debian11.dsc` in the nodes disk image field
in jFed or replace them directly in the `rspec` files.

## Automated Build

The image build can be done automatically by using the docker image in `../docker`.
Please refer to the README in there for instructions.
