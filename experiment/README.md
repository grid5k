# Experiment Setup

## Requirements

### jFed

jFed is needed to run an experiment with the current setup. Please see [here](https://jfed.ilabt.imec.be/)..

## Files

* experiment-specitication.yml: [ESpec](https://jfed.ilabt.imec.be/espec/) for jFed 
* taler.rspec: Complete set of nodes to run an experiment (others contain more wallets
  or shards for example). Find more in `../aditional/rspecs`
* env: template file to add enviroment variables needed for the experiment
* scripts: Bash scripts which will be run in the experiment
* ssh: ssh key material which the nodes use in the ESpec phase to communicate with each other.
  Safe to use here, since nodes can only be reached from inside the grid already (currently used
  for centos8 DB only)
  
## Run an Experiment

To successfully run an experiment the following steps must be made:

**NOTE** An external Grafana instance with Taler Performance Dashboards is needed (optionally) to see metrics and results.
         The Dashboards can be found in `additional/grafana`
         Install instructions can be found on [grafana.com](https://grafana.com/docs/grafana/latest/installation/)
         Once installed, two datasources must be added - Prometheus and Loki, they will be updated from the experiment

* Copy the environment default configuration `env` to `.env`
* Read through `.env` and define the missing variables **NOTE**: Postgres configuration is located in `scripts/database[-centos].sh`
* Start jFed Experimenter GUI 
* Load taler.rspec and click Run
* Specify the experiment name and time
* Wait until taler.rspec is allocated successfully and nodes are ready
* Click (Re)Run ESpec for the job (use the type Directory and select this directory (experiment))
* If any error ocurrs just press (Re)Run Espec again because sometimes there are still unidentified errors in jFed / G5k
* Start wallet processes with `taler-perf start wallet N` on any node in the experiment

**NOTE** The Grid'5000 environments are copied to a public directory of the grid. So it might still be
         available with `http://public.lille.grid5000.fr/~bfhch01/taler-debian11.dsc`
         If not you must build your own (see `../image`) and specify this one for each node in each rspec.
         This can be done manually via jFed (double click node, and replace `bfhch01` with your Grid'5000 username,
         Or with sed (which is simpler and faster):

```bash
sed -i "s/bfhch01/YOUR_G5K_USERNAME/g" <RSPEC_FILE>
```

### Hints and Bugs

#### Dependencies

Nodes may have dependencies in `run.sh`, thus waiting for another node to finish before continuing initialization.
An example of this is the exchange, it will only be started once `DB_USER` has remote access enabled (`wait_for_db` in `helpers.sh`).

However, most nodes which log to Promtail/Loki will wait with initialization until Promtail is running, so if something is stuck
execute the following command on the `monitor` node:

```bash
systemctl status promtail.service
```
or
```bash
cat /var/log/syslog | grep -i promtail
```

#### DNS not working

It _should_ work, if not just (RE)run ESpec, it happened sometimes that bind was not ready when we are setting the domain names with dyndns.

## Rebuild Taler Binaries

On each experiment start the `setup` script checks if variables like `GNUNET_COMMIT_SHA` are set (`.env`),
if they are, the corresponding binary is rebuilt from source with the specified commit.
For more info please read `scripts/setup.sh` or `scripts/install.sh`

If you forgot to set the variables, then `taler-perf rebuild` can do the work in a running experiment. But
processes would have to be restarted afterwards.

## Actions in a running Experiment

### Start wallets

* Run `talet-perf start wallet N` where N is any number

**NOTE** On `taler-perf`, when not using a terminal opened from jFed make sure to forward the ssh-agent
         to make the script work. E.g. `ssh -A graoully-3.nancy.grid5000.fr` (from an access machine).

To add more exchange processes run `taler-perf start exchange <NUM>` on any node
To add more wirewatch processes run `taler-perf start wirewatch <NUM>` on any node
To add more wallet processes run `taler-perf start wallet <NUM>` on any node

They can also be stopped in the same way: `taler-perf stop <KIND> <NUM>`

### Experiment Persistence

**Please make sure that important panel changes in Grafana are saved into the json files in `additonal/grafana`**
**This can be done clicking the share button in the top left corner of a dashboard then `Export` enable `Export for sharing externally` and finally `Save to file`.**

The script `../additional/persist.sh` can be used to backup and clean the data in the grid5k NFS.
This archive created can then be passed to `../additional/recover/run.sh`, which will run a local Grafana setup
in which the experiment can be inspected again.

For this to work the service `taler-databackup` using `scripts/prometheus-backup.sh` is run periodically.
Loki data is directly written to the NFS. For the best results it is needed to stop loki on the `monitor` node
before an experiment ends - otherwise some of the loki data might become corrupt.

#### (Deprecated) Grafana Dashboard Plotting

To persists the dashboards as png plots, there is a script in `../additional` which creates png
plots based on a configuration. Please refer to the README located in the specified directory.


### Developer Notes

#### Experiment Flow

1. `experiment-specification.yml` is run when running ESpec, setting up requirements and uploading scripts
2. `setup.sh` is run on every node, exporting necessary environment variables to `~/.env` and /etc/environment
3. `run.sh` is run on every node, setting domain names and starting the correct script according to the role the node has assigned 
   (given from `NODES` and `HOSTNAME`)

#### Add more nodes

Adding more nodes should be as simple as:

1. Adding it in an rspec file (jFed)
2. Adding the node name given in jFed to the `NODES` variable in `env` & `env`
3. Extending `run.sh` - add a section like: 
   ```bash
   ...
   elif [[ "${HOSTNAME}" =~ ${<NODE_NAME_UPPERCASE>_HOSTS} ]]; then
     ...
     exec ~/scripts/<node-script>.sh
   ...
   ```
4. Creating the `<node-script>.sh` containing node specific setup steps

#### Environment

That all environment variables are always available in every shell, they are exported to `/etc/environment`.
This file is also used by most of our custom service files in `/usr/lib/systemd/system`.

#### Useful functions

The file `scripts/helpers.sh` contains a lot of re-usable functions - please read this file for more information.

