#!/bin/bash
# Run the experiment
# Will determine the role of the node based on its hostname,
# set the DNS A record and start role specific scripts

set -euax
source ~/scripts/helpers.sh

if [[ "${ENABLE_EXPORTERS}" == "true" ]]; 
then
  systemctl restart prometheus-node-exporter
fi

# Set the experiment domain name
set_ddn ${NODE_NAME}.${DNS_ZONE}
set_host ${NODE_NAME}

# Some applications log to different promtail ports
# so that we have less log lines to filter when
# calculating prometheus metrics or other things with promtail
# (default for all is 1514) - see 'configs/etc/monitor/promtail.yaml'
PROMTAIL_LOG_PORT=1514

if [[ "${HOSTNAME}" =~ ${BANK_HOSTS} ]]; then 
    PROMTAIL_LOG_PORT=1518
    setup_log
    enable_logrotate
    exec ~/scripts/bank.sh init
elif [[ "${HOSTNAME}" =~ ${DB_HOSTS} ]]; then 
    PROMTAIL_LOG_PORT=1515
    setup_log
    enable_logrotate
    if grep -q "Red Hat" /proc/version; then 
      # Postgres is run differently in centos
      exec ~/scripts/database-centos.sh init
    else
      exec ~/scripts/database.sh init
    fi
elif [[ "${HOSTNAME}" =~ ${SHARD_HOSTS} ]]; then 
    setup_log
    enable_logrotate
    exec ~/scripts/shard.sh init
elif [[ "${HOSTNAME}" =~ ${EXCHANGE_HOSTS} ]]; then 
    setup_log
    enable_logrotate
    enable_netdelay db.${DNS_ZONE}
    if [[ "${PRIMARY_EXCHANGE}" =~ "${NODE_NAME}" ]]; then
      exec ~/scripts/exchange.sh init-primary
    else
      exec ~/scripts/exchange.sh init-secondary
    fi
elif [[ "${HOSTNAME}" =~ ${AGGREGATOR_HOSTS} ]]; then 
    setup_log
    enable_logrotate
    enable_netdelay db.${DNS_ZONE}
    exec ~/scripts/exchange-aggregator.sh init
elif [[ "${HOSTNAME}" =~ ${CLOSER_HOSTS} ]]; then 
    setup_log
    enable_logrotate
    enable_netdelay db.${DNS_ZONE}
    exec ~/scripts/exchange-closer.sh init
elif [[ "${HOSTNAME}" =~ ${TRANSFER_HOSTS} ]]; then 
    setup_log
    enable_logrotate
    enable_netdelay db.${DNS_ZONE}
    exec ~/scripts/exchange-transfer.sh init
elif [[ "${HOSTNAME}" =~ ${WIREWATCH_HOSTS} ]]; then 
    setup_log
    enable_logrotate
    enable_netdelay db.${DNS_ZONE}
    exec ~/scripts/exchange-wirewatch.sh init
elif [[ "${HOSTNAME}" =~ ${AUDITOR_HOSTS} ]]; then 
    setup_log
    enable_logrotate
    exec ~/scripts/auditor.sh init
elif [[ "${HOSTNAME}" =~ ${MERCHANT_HOSTS} ]]; then 
    setup_log
    enable_logrotate
    exec ~/scripts/merchant.sh init
elif [[ "${HOSTNAME}" =~ ${MONITOR_HOSTS} ]]; then 
    exec ~/scripts/monitor.sh init
elif [[ "${HOSTNAME}" =~ ${EPROXY_HOSTS} ]]; then 
    PROMTAIL_LOG_PORT=1516
    # Single domain for DNS load balancing
    set_ddn ${EXCHANGE_GW_DOMAIN}
    # We need a second domain for monitoring to
    # be able to get information about all proxies
    enable_netdelay ${PRIMARY_EXCHANGE}
    setup_log
    enable_logrotate
    exec ~/scripts/exchange-proxy.sh init
elif [[ "${HOSTNAME}" =~ ${DNS_HOSTS} ]]; then 
    exec ~/scripts/dns.sh
else
    # Be a wallet if no other node matched
    PROMTAIL_LOG_PORT=1517
    enable_netdelay ${EXCHANGE_GW_DOMAIN}
    setup_log
    exec ~/scripts/wallet.sh init
fi

exit 0
