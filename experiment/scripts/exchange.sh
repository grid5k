#!/bin/bash
INFO_MSG="
Setup the Exchange node
Start taler-exchange-httpd

Each exchange-http daemon, will get its own port starting from 10000
Detects it there are seperate nodes for aggregator and co., if not
the processes are started too.
"
OPT_MSG="
init-primary:
  Initialize the 'master exchange' and start NUM_EXCHANGE_PROCESSES
  exchange processes
  Sets up key material on the NFS to use for the secondary exchanges

init-secondary:
  Initialize the 'slave exchange' and start the processes
  Waits until the master is done and then starts
  NUM_EXCHANGE_PROCESSES exchange processes.

start NUM:
  Start another NUM exchange-http daemons

stop NUM:
  Stop NUM exchange-httpd daemons

add-auditor AUDITOR_PUB_KEY AUDITOR_API_URL AUDITOR_NAME:
  Register an auditor to the exchange
  Best called from the auditor node.

"

set -eux
source ~/scripts/helpers.sh

# Setup the configuration in /etc/taler
function setup_primary_config() {

  # remove keys from previous run in same experiment
  rm -rf /var/lib/taler/exchange-offline/*

  # Setup the base configuration (helpers.sh)
  setup_exchange_config_without_master_key "http://${NODE_NAME}.${DNS_ZONE}:10000/"

  MASTER_KEY=$(sudo -u taler-exchange-offline taler-exchange-offline setup)

  sed -i -e "s/<MASTER_KEY_HERE>/${MASTER_KEY}/g" \
  	/etc/taler/conf.d/exchange-business.conf

}

# Check if there are exchange-* processes configured to be run
# on external systems, if not start them here.
function start_other_exchange_binaries() {
  if [[ "${AGGREGATOR_HOSTS}" == "none" ]]; then
    source ~/scripts/exchange-aggregator.sh init-start
  fi
  if [[ "${CLOSER_HOSTS}" == "none" ]]; then
    source ~/scripts/exchange-closer.sh init-start
  fi
  if [[ "${TRANSFER_HOSTS}" == "none" ]]; then
    source ~/scripts/exchange-transfer.sh init-start
  fi
  if [[ "${WIREWATCH_HOSTS}" == "none" ]]; then
    source ~/scripts/exchange-wirewatch.sh init-start
  fi
}

# Setup the exchange with the taler-exchange-offline signing procedure
function setup_primary_exchange() {

  # Setup the shared key directory when we use a secondary node
  if [[ ${NUM_EXCHANGES} != "1" ]]; then
    rm -rf /home/${G5K_USER}/taler || true
    mkdir -p /home/${G5K_USER}/taler/exchange-secmod-{cs,rsa,eddsa}
  fi

  systemctl restart taler-exchange-httpd@10000.service

  start_other_exchange_binaries

  wait_for_keys "${PRIMARY_EXCHANGE}:10000/management"

  sleep 5

  taler-exchange-offline download > sig-req.json
  taler-exchange-offline sign < sig-req.json > sig-res.json
  taler-exchange-offline enable-account "payto://x-taler-bank/bank.${DNS_ZONE}/exchange?receiver-name=exchange" > acct-res.json
  taler-exchange-offline wire-fee $(date +%Y) x-taler-bank KUDOS:0 KUDOS:0 > fee-res.json
  taler-exchange-offline upload < sig-res.json
  taler-exchange-offline upload < acct-res.json
  taler-exchange-offline upload < fee-res.json
}

# Initialize all stuff needed
# logs, configs, exchanges
# For the primary node which is responsible for key creation
function init_primary_exchange() {
  restart_rsyslog
  setup_primary_config
  wait_for_db
  setup_primary_exchange
}

# Initialize all stuff needed for secondary exchange nodes
# They use the key material from the primary exchange
function init_secondary_exchange() {
  restart_rsyslog
  setup_exchange_config_master_key_from_api
}

# Start N new exchange-http daemons
# $1: N - number of currently running exchanges
# $1: N - number of new exchanges to start
function start_exchanges() {

  let "START=$1+10000"
  let "END=$START+$2-1"

  for PORT in $(seq $START $END); do
    systemctl restart taler-exchange-httpd@"${PORT}".socket \
                      taler-exchange-httpd@"${PORT}".service
    # Wait so they have some small delay in between their routines from the start
    sleep 0.05
  done
}

# Stop N exchange daemons
# $1: N -- number of exchanges to stop
function stop_exchanges() {
  stop_numbered_services "taler-exchange-httpd" $1
}

case $1 in
  init-primary)
    init_primary_exchange
    start_exchanges "1" "$((${NUM_EXCHANGE_PROCESSES}-1))"
    ;;
  init-secondary)
    init_secondary_exchange
    start_exchanges "0" "${NUM_EXCHANGE_PROCESSES}"
    ;;
  start)
    RUNNING=$(ps -aux | grep "[taler]-exchange-httpd" | wc -l)
    start_exchanges $RUNNING $2
    ;;
  stop)
    stop_exchanges $2
    ;;
  add-auditor)
    taler-exchange-offline enable-auditor $2 $3 "$4" > auditor.json
    taler-exchange-offline upload < auditor.json
    ;;
  *)
    taler_perf_help $0 "$INFO_MSG" "$OPT_MSG"
    ;;
esac

exit 0
