#!/bin/bash
INFO_MSG="
Confiure and start the nginx proxy server
"
OPT_MSG="
init:
  Initialize and start the proxy with NUM_EXCHANGE_PROCESSES upstreams

start NUM:
  Add another NUM exchanges to the list of upstreams

stop NUM:
  Remove NUM exchanges from the list of upstreams
"

set -eux
source ~/scripts/helpers.sh

# Add N exchanges to the upstream servers in the proxy configuration
# $1: N - Number of currently running exchanges (per exchange host)
# $2: N - Number of exchanges to add
function add_exchanges() {

  let "START=$1+10000"
  let "END=$START+$2-1"

  EXCHANGES=$(get_hosts "exchange-")

  for PORT in $(seq $START $END); do
    for HOST in ${EXCHANGES}; do
      sed -i -e "/<SERVERS_HERE>/a \ \ server ${HOST}.${DNS_ZONE}:${PORT};" \
              /etc/nginx/sites-available/exchange
    done
  done
}

# Setup the node and proxy configuration
function setup_config() {

  sed -i -e "s/<EXCHANGE_GW_DOMAIN_HERE>/${EXCHANGE_GW_DOMAIN}/g" \
         /etc/nginx/sites-available/exchange

  
  add_exchanges "0" ${NUM_EXCHANGE_PROCESSES}

  setup_rsyslog_for_nginx
  
  # Allow enough files to be opened by nginx (www-data)
  echo "
  fs.file-max=500000
  " >> /etc/sysctl.conf
  
  echo "
  www-data soft nofile unlimited
  www-data hard nofile unlimited
  " >> /etc/security/limits.conf
  
  sysctl -p
}

# Initialize and start the proxy
function init_proxy() {
  create_cert "${EXCHANGE_GW_DOMAIN}" "/etc/ssl/proxy"
  setup_config

  # Setup the directory where Ngxinx will place its cache
  # configured in <g5k>/configs/etc/nginx/sites-availabled/exchange
  mkdir -p /var/cache/proxy

  restart_rsyslog
  ln -sf /etc/nginx/sites-available/exchange /etc/nginx/sites-enabled/exchange
  
  # Nginx does not start until the destination server is reachable - wait here
  # nginx: [emerg] host not found in upstream "exch.perf.taler" ...
  wait_for_keys "${PRIMARY_EXCHANGE}:10000"
  
  systemctl restart nginx \
	            prometheus-nginx-exporter
}

# Remove N exchanges from the upstream list
# $1: N - number of currently running exchanges
# $2: N - number of exchanges to remove
function remove_exchanges() {

  # We know we started from port 10000
  # get the highest port numbers and start removing from there
  let "START=$1+10000"
  let "END=$START-$2"

  EXCHANGES=$(get_hosts "exchange-")

  for PORT in $(seq $END $START); do
    for HOST in ${EXCHANGES}; do
      sed -i "/${HOST}.${DNS_ZONE}:${PORT};/d" \
            /etc/nginx/sites-available/exchange
    done
  done
}

case $1 in
  init)
    init_proxy
    ;;
  start)
    add_exchanges $2 $3
    ;;
  stop)
    remove_exchanges $2 $3
    ;;
  *)
    taler_perf_help $0 "$INFO_MSG" "$OPT_MSG"
    ;;
esac

systemctl reload nginx
