#!/bin/bash
INFO_MSG="
Configure and start taler-wallets
"
OPT_MSG="
init:
  Initialize the wallets and start an inital NUM_WALLET_PROCESSES

start NUM:
  Start NUM new wallet benchmark processes

stop NUM:
  Stop NUM wallet benchmark processes
"

set -eux
source ~/scripts/helpers.sh

# Enable rsyslog and wait until the exchange is ready
function init_wallets() {
  restart_rsyslog
  wait_for_keys "${EXCHANGE_GW_DOMAIN}"
  sleep 5
}

# Start N wallets
# If CPU load is over 75%, no wallets are started here
# $1 Number of wallets to start
function start_wallets() {
  CPU_USAGE=$[100-$(vmstat 1 2 | tail -1 | awk '{print $15}')]
  echo "CPU Usage: ${CPU_USAGE}%"

  if [ "${CPU_USAGE}" -gt "75" ]; then 
    echo "Not starting any more wallets"
    exit 1
  fi

  # count the running wallets so that numbers can be increased
  RUNNING=$(ps -aux | grep "[wallet]-cli" | wc -l)

  for i in $(seq ${1}); do
    let "i+=${RUNNING}"
    # This starts the benchmark.sh script with i as argument
    systemctl restart taler-wallet@${i}.service
    sleep 0.5
  done
}

# Stop N wallets
# $1: N - number of wallets to stop or "all"
function stop_wallets() {
  if [[ "$1" == "all" ]]; then
    systemctl stop taler-wallet@*.service
  else
    stop_numbered_services "taler-wallet" $1
  fi
}

case "$1" in
  init)
    init_wallets
    start_wallets ${NUM_WALLET_PROCESSES:-10}
    ;;
  stop)
    stop_wallets $2
    ;;
  start)
    start_wallets $2
    ;;
  *)
    taler_perf_help $0 "$INFO_MSG" "$OPT_MSG"
    ;;
esac

exit 0
