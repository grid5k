#!/bin/bash
INFO_MSG="
Setup the Exchange Wirewatch node
Start taler-exchange-wirewatch
"
OPT_MSG="
init:
  Initialize the application(s) and start them
  uses NUM_WIREWATCH_PROCESSES

init-start:
  Same as init but do not configure taler.conf,
  just start the service
"

set -eux
source ~/scripts/helpers.sh

# Start N new exchange-aggregator processes
# $1: N - number of new aggregators to start
function start_wirewatches() {
  # count the running wirewatches so that numbers can be increased
  RUNNING=$(ps -aux | grep "[taler]-exchange-wirewatch" | wc -l)

  for i in $(seq ${1}); do
    let "i+=${RUNNING}"
    systemctl restart taler-exchange-wirewatch@"${i}".service
    sleep 0.5
  done
}

function stop_wirewatches() {
  stop_numbered_services "taler-exchange-wirewatch" $1
}

case $1 in
  init)
    setup_exchange_config_master_key_from_api
    restart_rsyslog
    start_wirewatches "$NUM_WIREWATCH_PROCESSES"
    ;;
  init-start)
    restart_rsyslog
    start_wirewatches "$NUM_WIREWATCH_PROCESSES"
    ;;
  start)
    start_wirewatches "$2"
    ;;
  stop)
    stop_wirewatches "$2"
    ;;
  *)
    taler_perf_help $0 "$INFO_MSG" "$OPT_MSG"
    ;;
esac
