#!/bin/bash
INFO_MSG="
Start a wallet benchmark loop with 100'000 iterations

Normally started by taler-wallet@.service
"
OPT_MSG="
<N>: 
  Any number 
  If it is dividable by 100 then the wallet will log in INFO level
"

set -eu
source ~/scripts/helpers.sh

# Start a wallet benchmark loop
function start_wallet_bench() {

  PROTO=http
  if [[ ${WALLET_USE_HTTPS} == "true" ]]; then
    PROTO=https
  fi
  
  LOG_LEVEL=ERROR
  # One wallet in a hundred should log messages
  if ! (($1 % 100)) || [ $1 == "1" ]; then
    LOG_LEVEL=INFO
  fi

  if [[ "${RAND_DEPOSITS}" == "true" ]]; then
     NUM_DEPOSITS=$(($RANDOM % ${NUM_DEPOSITS} + 1))
  fi

  if [[ "${WALLET_BENCHMARK}" == bench3 ]] && \
     [[ "${WALLET_MERCHANT_SELECTION}" =~ zipf|rand ]]; then
    taler-wallet-cli \
      -L ${LOG_LEVEL} \
      advanced ${WALLET_BENCHMARK} \
      --config-json "
    {
      \"exchange\": \"${PROTO}://${EXCHANGE_GW_DOMAIN}/\",
      \"bank\": \"${PROTO}://bank.${DNS_ZONE}/\",
      \"currency\": \"KUDOS\",
      \"paytoTemplate\": \"payto://x-taler-bank/bank.${DNS_ZONE}/merchant-\${id}?receiver-name=merchant\",
      \"randomAlg\": \"${WALLET_MERCHANT_SELECTION}\",
      \"numMerchants\": ${WALLET_NUM_MERCHANTS},
      \"iterations\": 500,
      \"deposits\": ${NUM_DEPOSITS},
      \"restartAfter\": 2
    }"
  else
    if [[ "${WALLET_BENCHMARK}" == bench3 ]]; then
      WALLET_BENCHMARK=bench1
    fi
    taler-wallet-cli \
      -L ${LOG_LEVEL} \
      advanced ${WALLET_BENCHMARK} \
      --config-json "
    {
      \"exchange\": \"${PROTO}://${EXCHANGE_GW_DOMAIN}/\",
      \"bank\": \"${PROTO}://bank.${DNS_ZONE}/\",
      \"currency\": \"KUDOS\",
      \"payto\": \"payto://x-taler-bank/bank.${DNS_ZONE}/merchant-1?receiver-name=merchant-1\",
      \"iterations\": 500,
      \"deposits\": ${NUM_DEPOSITS},
      \"withdrawOnly\": ${WALLET_WITHDRAW_ONLY},
      \"restartAfter\": 2
    }"
  fi
}

case $1 in
  [0-9]*)
    start_wallet_bench $1
    ;;
  *)
    taler_perf_help $0 "$INFO_MSG" "$OPT_MSG"
    ;;
esac

exit 0
