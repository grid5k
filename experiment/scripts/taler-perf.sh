#!/bin/bash
# taler-perf utility script
# Start and stop processes
#
# Usage: ./taler-perf.sh
set -e

source ~/scripts/helpers.sh

function get_running_exchanges_per_host() {
  RUNNING=$(\
    ssh -o StrictHostKeyChecking=no ${PRIMARY_EXCHANGE} \
           '/bin/bash -c "ps -aux | grep [taler]-exchange-httpd | wc -l"' \
  )
  echo "${RUNNING}"
}

function start_wallets() {
  for WALLET in $(get_hosts "wallet"); do
    ssh -o StrictHostKeyChecking=no ${WALLET}.${DNS_ZONE} \
           "/bin/bash /root/scripts/wallet.sh start ${1:-${NUM_WALLET_PROCESSES}}" &
  done
  wait
}

function stop_wallets() {
 for WALLET in $(get_hosts "wallet"); do
   ssh -o StrictHostKeyChecking=no ${WALLET}.${DNS_ZONE} \
          "/bin/bash /root/scripts/wallet.sh stop ${1:-${NUM_WALLET_PROCESSES}}" &
  done
  wait
}

function start_exchanges() {
  RUNNING=$(get_running_exchanges_per_host)
  for HOST in $(get_hosts "exchange-"); do
    ssh -o StrictHostKeyChecking=no ${HOST}.${DNS_ZONE} \
           "/bin/bash /root/scripts/exchange.sh start ${1:-${NUM_EXCHANGE_PROCESSES}}" 
  done
  for HOST in $(get_hosts "eproxy"); do
    ssh -o StrictHostKeyChecking=no ${HOST}.${DNS_ZONE} \
           "/bin/bash /root/scripts/exchange-proxy.sh start ${RUNNING} ${1:-${NUM_EXCHANGE_PROCESSES}}"
  done
  ssh -o StrictHostKeyChecking=no "monitor.${DNS_ZONE}" \
         "/bin/bash /root/scripts/monitor.sh start ${RUNNING} ${1:-${NUM_EXCHANGE_PROCESSES}}"
}

function stop_exchanges() {
  # must remove the exchange form the monitor host before the one from the nginx
  # since helpers.sh gets all exchanges from the nginx config
  RUNNING=$(get_running_exchanges_per_host)
  ssh -A -o StrictHostKeyChecking=no "monitor.${DNS_ZONE}" \
         "/bin/bash /root/scripts/monitor.sh stop-exchanges ${RUNNING} ${1:-${NUM_EXCHANGE_PROCESSES}}"
  for HOST in $(get_hosts "eproxy"); do
    ssh -A -o StrictHostKeyChecking=no ${HOST}.${DNS_ZONE} \
           "/bin/bash /root/scripts/exchange-proxy.sh stop ${RUNNING} ${1:-${NUM_EXCHANGE_PROCESSES}}"
  done
  sleep 5
  for HOST in $(get_hosts "exchange-"); do
    ssh -o StrictHostKeyChecking=no ${HOST}.${DNS_ZONE} \
           "/bin/bash /root/scripts/exchange.sh stop ${1:-${NUM_EXCHANGE_PROCESSES}}" 
  done
}

function start_wirewatches() {
  ssh -A -o StrictHostKeyChecking=no "wirewatch.${DNS_ZONE}" \
         "/bin/bash /root/scripts/exchange-wirewatch.sh start ${1}"
}

function stop_wirewatches() {
  ssh -A -o StrictHostKeyChecking=no "wirewatch.${DNS_ZONE}" \
         "/bin/bash /root/scripts/exchange-wirewatch.sh stop ${1}"
}

function start_processes() {
  case "$1" in
    wallet)
      start_wallets $2
      ;;
    exchange)
      start_exchanges $2
      ;;
    wirewatch)
      start_wirewatches $2
      ;;
    *)
      echo "Unknown argument '$1' for function ${FUNCNAME[0]}"
      echo "Usage: start [wallet|exchange|wirewatch] NUM"
      ;;
  esac
}

function stop_processes() {
  case "$1" in 
    wallet)
      stop_wallets $2
      ;;
    exchange)
      stop_exchanges $2
      ;;
    wirewatch)
      stop_wirewatches $2
      ;;
    *)
      echo "Unknown argument '$1' for function ${FUNCNAME[0]}"
      echo "Usage: stop [wallet|exchange|wirewatch] NUM"
      ;;
   esac
}

function rebuild() {
  while [[ $# -gt 0 ]]; do
    case "$1" in
      --libmicrohttpd|-m)
        LIBMICROHTTPD_COMMIT_SHA="$2"
        shift 2
        ;;
      --libmicro-cflags|-lc)
	LIBMICROHTTPD_CFLAGS="$2"
	shift 2
	;;
      --exchange|-e)
        EXCHANGE_COMMIT_SHA="$2"
        shift 2
        ;;
      --exch-cflags|-ec)
	EXCHANGE_CFLAGS="$2"
	shift 2
	;;
      --gnunet|-g)
        GNUNET_COMMIT_SHA="$2"
        shift 2
        ;;
      --gnunet-cflags|-gc)
	GNUNET_CFLAGS="$2"
	shift 2
	;;
      --wallet|-w)
        WALLET_COMMIT_SHA="$2"
        shift 2
        ;;
      --merchant|-m)
        MERCHANT_COMMIT_SHA="$2"
        shift 2
        ;;
      --merch-cflags|-mc)
	MERCHANT_CFLAGS="$2"
	shift 2
	;;
      *)
        echo "Unkown argument $1"
        echo "Usage rebuilt [target] [cflags]"
        echo "Targets: "
        echo "-e|--exchange <commit-sha>"
        echo "-g|--gnunet <commit-sha>"
        echo "-w|--wallet <commit-sha>"
        echo "-m|--merchant <commit-sha>"
	echo "Cflags: "
	echo "-lc|--libmicro-cflags <cflags>"
	echo "-gc|--gnunet-cflags <cflags>"
	echo "-ec|--exch-cflags <cflags>"
	echo "-mc|--merch-cflags <cflags>"
        exit 1
    esac
  done
  exec ~/scripts/install.sh
}

function rebuild_all() {
  for NODE in $(dig -t AXFR ${DNS_ZONE} | grep "\<A\>" | awk '{print substr($1, 1, length($1)-1)}'); do
    if [[ $NODE == $EXCHANGE_GW_DOMAIN ]]; then continue; fi
    ssh -o StrictHostKeyChecking=no ${NODE} "taler-perf build $(printf "%q " "$@")" &
  done
  wait
}

case "$1" in
  start)
    shift
    start_processes $@
    ;;
  stop)
    shift
    stop_processes $@
    ;;
  rebuild)
    shift
    rebuild_all "$@"
    ;;
  build)
    shift
    rebuild "$@"
    ;;
  *)
    echo "Usage:"
    echo "start|stop|build|rebuild_all"
    echo "just run a command without an argument to get help"
    exit 1
    ;;
esac

exit 0
