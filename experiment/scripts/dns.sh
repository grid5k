#!/bin/bash
set -eux

# Backup used nodes for experiment
cp ~/nodes.json ${LOG_DIR}/nodes.json

if [[ "$REMOVE_PREVIOUS_EXPERIMENT_DATA" == "true" ]]; then
  rm -rf /home/${G5K_USER}/espec-times || true
fi
if ! grep -q "# Times" /home/${G5K_USER}/espec-times; then
  echo "# Times to use for recovery" > /home/${G5K_USER}/espec-times
fi
echo "$(date +%s)" >> /home/${G5K_USER}/espec-times

# Clean the file so it is cleanly updated in each run
rm ${LOG_DIR}/commits.txt || true

for DIR in $(find ~/taler -type d -maxdepth 1); do
  cd ${DIR}
  echo "${DIR}: $(git rev-parse HEAD)" >> ${LOG_DIR}/commits.txt
  cd -
done
