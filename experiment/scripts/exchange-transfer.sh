#!/bin/bash
INFO_MSG="
Setup the Exchange Transfer node
Start taler-exchange-transfer
"
OPT_MSG="
init:
  Initialize the application(s) and start them
  uses NUM_TRANSFER_PROCESSES

init-start:
  Same as init but do not configure taler.conf,
  just start the service
"

set -eux
source ~/scripts/helpers.sh

# Start N new exchange-aggregator processes
# $1: N - number of new aggregators to start
# NOTE: only for init purposes currently
function start_transfers() {
  restart_rsyslog
  for i in $(seq ${1}); do
    systemctl restart taler-exchange-transfer@"${i}".service
  done
}

case $1 in
  init)
    setup_exchange_config_master_key_from_api
    start_transfers "$NUM_TRANSFER_PROCESSES"
    ;;
  init-start)
    start_transfers "$NUM_TRANSFER_PROCESSES"
    ;;
  *)
    taler_perf_help $0 "$INFO_MSG" "$OPT_MSG"
    ;;
esac
