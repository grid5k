#!/bin/bash
# Create all necessary users and groups 
# for the taler applications.
# (normaly done automatically when installing from packages)
#
# Usage: ./createusers.sh
set -ex

DEBIAN_FRONTEND=noninteractive
source /usr/share/debconf/confmodule

TALER_HOME="/var/lib/taler"

GROUPNAME=taler-exchange-secmod
DBGROUPNAME=taler-exchange-db
EUSERNAME=taler-exchange-httpd
EGROUPNAME=www-data
OUSERNAME=taler-exchange-offline
OGROUPNAME=taler-exchange-offline
CLOSERUSERNAME=taler-exchange-closer
RSECUSERNAME=taler-exchange-secmod-rsa
ESECUSERNAME=taler-exchange-secmod-eddsa
CSECUSERNAME=taler-exchange-secmod-cs
AGGRUSERNAME=taler-exchange-aggregator
WIREUSERNAME=taler-exchange-wire
MERCHUSERNAME=taler-merchant-httpd
MERCHGROUPNAME=www-data

AUDITCONFIG_FILE="/etc/default/taler-auditor"
AUDITTALER_HOME="/var/lib/taler-auditor"
AUDITUSERNAME=taler-auditor-httpd
AUDITGROUPNAME=www-data
AOUSERNAME=taler-auditor-offline
AOGROUPNAME=taler-auditor-offline

# Create taler groups as needed
if ! getent group ${GROUPNAME} >/dev/null; then
  addgroup --quiet --system ${GROUPNAME}
fi
if ! getent group ${DBGROUPNAME} >/dev/null; then
  addgroup --quiet --system ${DBGROUPNAME}
fi
if ! getent group ${OGROUPNAME} >/dev/null; then
  addgroup --quiet --system ${OGROUPNAME}
fi

# Create taler users if needed
if ! getent passwd ${EUSERNAME} >/dev/null; then
  adduser --quiet --system \
    --no-create-home \
    --ingroup ${GROUPNAME} \
    --home ${TALER_HOME} ${EUSERNAME}
  adduser --quiet ${EUSERNAME} ${DBGROUPNAME}
fi
if ! getent passwd ${RSECUSERNAME} >/dev/null; then
  adduser --quiet --system \
    --no-create-home \
    --ingroup ${GROUPNAME} \
    --home ${TALER_HOME} ${RSECUSERNAME}
fi
if ! getent passwd ${ESECUSERNAME} >/dev/null; then
  adduser --quiet --system \
    --no-create-home \
    --ingroup ${GROUPNAME} \
    --home ${TALER_HOME} ${ESECUSERNAME}
fi
if ! getent passwd ${CSECUSERNAME} >/dev/null; then
  adduser --quiet --system \
    --no-create-home \
    --ingroup ${GROUPNAME} \
    --home ${TALER_HOME} ${CSECUSERNAME}
fi
if ! getent passwd ${WIREUSERNAME} >/dev/null; then
  adduser --quiet --system \
    --no-create-home \
    --home ${TALER_HOME} ${WIREUSERNAME}
  adduser --quiet ${WIREUSERNAME} ${DBGROUPNAME}
fi
if ! getent passwd ${CLOSERUSERNAME} >/dev/null; then
  adduser --quiet --system \
    --no-create-home \
    --home ${TALER_HOME} ${CLOSERUSERNAME}
  adduser --quiet ${CLOSERUSERNAME} ${DBGROUPNAME}
fi
if ! getent passwd ${AGGRUSERNAME} >/dev/null; then
  adduser --quiet --system \
    --no-create-home \
    --home ${TALER_HOME} ${AGGRUSERNAME}
  adduser --quiet ${AGGRUSERNAME} ${DBGROUPNAME}
fi
if ! getent passwd ${OUSERNAME} >/dev/null; then
  adduser --quiet --system \
    --ingroup ${OGROUPNAME} \
    --no-create-home \
    --home ${TALER_HOME} ${OUSERNAME}
fi
if ! getent passwd ${MERCHUSERNAME} >/dev/null; then
  adduser --quiet --system \
    --no-create-home \
    --ingroup ${MERCHGROUPNAME} \
    --home ${TALER_HOME} ${MERCHUSERNAME}
fi

install -d /var/lib/taler/exchange-offline -m 0700 -o ${OUSERNAME} -g ${OGROUPNAME}
install -d /run/taler/exchange-secmod-rsa -m 0755 -o ${RSECUSERNAME} -g ${GROUPNAME}
install -d /run/taler/exchange-secmod-eddsa -m 0755 -o ${ESECUSERNAME} -g ${GROUPNAME}
install -d /run/taler/exchange-secmod-cs -m 0755 -o ${CSECUSERNAME} -g ${GROUPNAME}
install -d /run/taler/exchange-httpd -m 0750 -o ${EUSERNAME} -g ${EGROUPNAME}
install -d /var/lib/taler/exchange-offline -m 0700 -o ${OUSERNAME} -g ${OGROUPNAME}
install -d /var/lib/taler/exchange-secmod-rsa -m 0700 -o ${RSECUSERNAME} -g ${GROUPNAME}
install -d /var/lib/taler/exchange-secmod-eddsa -m 0700 -o ${ESECUSERNAME} -g ${GROUPNAME}
install -d /var/lib/taler/exchange-secmod-cs -m 0700 -o ${CSECUSERNAME} -g ${GROUPNAME}
install -d /run/taler/merchant-httpd -m 0755 -o ${MERCHUSERNAME} -g ${MERCHGROUPNAME}

if ! dpkg-statoverride --list /etc/taler/secrets/exchange-accountcredentials.secret.conf >/dev/null 2>&1; then
  dpkg-statoverride --add --update \
    ${WIREUSERNAME} root 460 \
    /etc/taler/secrets/exchange-accountcredentials.secret.conf
fi

if ! dpkg-statoverride --list /etc/taler/secrets/exchange-db.secret.conf >/dev/null 2>&1; then
  dpkg-statoverride --add --update \
    root ${DBGROUPNAME} 660 \
    /etc/taler/secrets/exchange-db.secret.conf
fi

if ! dpkg-statoverride --list /etc/taler/secrets/merchant-db.secret.conf >/dev/null 2>&1; then
  dpkg-statoverride --add --update \
    ${MERCHUSERNAME} root 460 \
    /etc/taler/secrets/merchant-db.secret.conf
fi

# Auditor 
if ! getent group ${AUDITGROUPNAME} >/dev/null; then
  addgroup --quiet --system \
           ${AUDITGROUPNAME}
fi
if ! getent group ${AOGROUPNAME} >/dev/null; then
  addgroup --quiet --system ${AOGROUPNAME}
fi

if ! getent passwd ${AUDITUSERNAME} >/dev/null; then
  adduser --quiet --system \
          --ingroup ${AUDITGROUPNAME} \
	  --no-create-home \
	  --home ${AUDITTALER_HOME} \
	  ${AUDITUSERNAME}
fi

if ! dpkg-statoverride --list /etc/taler/secrets/auditor-db.secret.conf >/dev/null 2>&1; then
  dpkg-statoverride --add --update \
    ${AUDITUSERNAME} ${AUDITGROUPNAME} 660 \
    /etc/taler/secrets/auditor-db.secret.conf
fi
if ! getent passwd ${AOUSERNAME} >/dev/null; then
  adduser --quiet --system \
    --ingroup ${AOGROUPNAME} \
    --no-create-home \
    --home ${TALER_HOME} ${AOUSERNAME}
fi

install -d /run/taler/auditor-httpd -m 0755 -o ${AUDITUSERNAME} -g ${AUDITGROUPNAME}
install -d /var/lib/taler/auditor -m 0700 -o ${AOUSERNAME} -g ${AOGROUPNAME}
