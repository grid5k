#!/bin/bash
INFO_MSG="
Setup the bank node for the experiments
(Start the taler-fakebank)
"
OPT_MSG="
init: 
  Configure and start the taler-fakebank
"

set -eux
source ~/scripts/helpers.sh

# Start the taler-fakebank
function init_bank() {
  echo "
fs.file-max=500000
  " >> /etc/sysctl.conf
  
  echo "
* soft nofile unlimited
* hard nofile unlimited
  " >> /etc/security/limits.conf
  
  sysctl -p

  # Force the "Make tranfer from X to Y" to be logged for statistics
  # of payment distribution
  echo "GNUNET_FORCE_LOG=\";;make_transfer;;INFO\"" \
    | tee -a /etc/environment ~/.env

  create_cert "${NODE_NAME}.${DNS_ZONE}" "/etc/ssl/bank"
  setup_rsyslog_for_nginx

  restart_rsyslog

  ln -sf /etc/nginx/sites-available/fakebank /etc/nginx/sites-enabled/fakebank
  systemctl restart taler-fakebank.service \
                    nginx
}

case $1 in
  init)
    init_bank
    ;;
  *)
    taler_perf_help $0 "$INFO_MSG" "$OPT_MSG"
    ;;
esac

exit 0
