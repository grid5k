#!/bin/bash
INFO_MSG="
Measure and log round-trip-times between experiment nodes
Logs to promtail directly

Used by taler-netdelay.service
"
OPT_MSG="
<DEST>:
  Destination host to ping
"

source ~/scripts/helpers.sh

if [[ -z $1 ]]; then
  taler_perf_help $0 "$INFO_MSG" "$OPT_MSG"
  exit 2
fi

RTT=$(\
  ping -c 2 ${1} | \
  sed -n "3p" | \
  awk '{print $(NF-1)$NF}' \
)

if [[ "${RTT}" == "time="* ]]; 
then
  logger -s --tcp \
	 --port 1514 \
	 --server "monitor.${DNS_ZONE}" \
         --tag taler-network \
	 "src=${TALER_HOST} dst=${1} ${RTT}" \
	 || true # Ignore errors (mostly because NXDOMAIN)
fi
