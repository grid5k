## Postgres Cluster Experiment

**UNMAINTAINED** Used for some experiments hosting multiple instances of postgres on the same node. 
                 They may still work but are not actively maintained.

Run in the following order after espec was run (example for two instances):

**NOTE** Requires as many exchange and proxy nodes as postgres instances are created
         Example for 2 postgres instances: `DB`, `Exchange-1`, `Exchange-2`, `Proxy-1`, `Proxy-2`

1. Modify scripts/benchmark.sh to be 'randomly using only one exchange for each wallet`. e.g. add this line
   before taler-wallet-cli is called:
   ```bash
   EXCHANGE_GW_DOMAIN="exchange-$(shuf -i 1-3 -n 1).${DNS_ZONE}"
   ```
2. Run Espec
3. Create a second postgres instance on the DB node: `./db-cluser.sh 1`
4. Initialize one Exchange node as a primary exchange for this db (`Exchange-2`): `./exch-cluster 1`
5. Configure all proxies to be responsible for one exchange only:
   `Proxy-1`: `./proxy-cluster 2` - deletes all exchange-2 entries from the nginx config
   `Proxy-2`: `./proxy-cluster 1` - ----------  exchange-1 -----------------------------
6. Start wallets  

This can be done with as many instances as required.
