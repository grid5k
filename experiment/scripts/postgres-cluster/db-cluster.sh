#!/bin/bash
# Create independent postgres instances (cluster)

if [[ -z ${1} ]]; then
  echo "Usage: ./db-cluster.sh <N>"
  echo ""
  echo "Creates an independent Postgres instance main<N> running on port 5432+N"
  echo "Call after espec was run - only works with partitioning"
  exit 2
fi

PORT=$((5432 + ${1}))

pg_createcluster 13 main${1} -p $PORT

mv /var/lib/postgresql/13/main${1} /tmp/postgresql/13/

if [ -d /mnt/disk ]; then
  mv /tmp/postgresql/13/main${1}/pg_wal /mnt/disk/pg_wal${1}
  ln -sf /mnt/disk/pg_wal${1} /tmp/postgresql/13/main${1}/pg_wal
  chown postgres:postgres /tmp/postgresql/13/main${1}/pg_wal
fi

cp /etc/postgresql/13/main/exchange.conf /etc/postgresql/13/main/pg_hba.conf /etc/postgresql/13/main${1}
sed -i "s|/tmp/postgresql/13/main|/tmp/postgresql/13/main${1}|g" /etc/postgresql/13/main${1}/exchange.conf
echo "include = 'exchange.conf'" >> /etc/postgresql/13/main${1}/postgresql.conf

systemctl restart postgresql@13-main${1}.service

echo "
[exchangedb-postgres]
CONFIG=postgres://:${PORT}/${DB_NAME}
" > /etc/taler/secrets/exchange-db.secret.conf

su postgres << EOF
psql postgres -p ${PORT} -tAc "DROP DATABASE IF EXISTS \"${DB_NAME}\";"
psql postgres -p ${PORT} -tAc "SELECT 1 FROM pg_roles WHERE rolname='taler-exchange-httpd'" | \
  grep -q 1 || \
  createuser -p ${PORT} taler-exchange-httpd
psql -p ${PORT} -tAc "SELECT 1 FROM pg_database WHERE datname='${DB_NAME}'" | \
  grep -q 1 || \
  createdb -p ${PORT} -O taler-exchange-httpd "${DB_NAME}"
psql -p ${PORT} -tAc "CREATE EXTENSION IF NOT EXISTS pg_stat_statements"
EOF

su postgres << EOF
psql postgres -p ${PORT} -tAc "SELECT 1 FROM pg_roles WHERE rolname='${DB_USER}'" | \
  grep -q 1 || \
  psql -p ${PORT} << END
    CREATE USER "${DB_USER}" with encrypted password '${DB_PASSWORD}';
END
EOF

sudo -u taler-exchange-httpd taler-exchange-dbinit -r || true
sudo -u taler-exchange-httpd taler-exchange-dbinit -s || true
sudo -u taler-exchange-httpd taler-exchange-dbinit -P ${NUM_PARTITIONS}

su taler-exchange-httpd -s /bin/bash << EOF
psql -p ${PORT} -d "${DB_NAME}"
GRANT SELECT,INSERT,UPDATE ON ALL TABLES IN SCHEMA public TO "${DB_USER}";
GRANT USAGE ON ALL SEQUENCES IN SCHEMA public TO "${DB_USER}";
EOF

ssh -o StrictHostKeyChecking=no monitor.${DNS_ZONE} \
       "sed -i \"s/DATA_SOURCE_NAME.*'$//\" /etc/default/prometheus-postgres-exporter && 
        sed -i \"s|DATA_SOURCE_NAME.*|&,postgresql://postgres@db.${DNS_ZONE}:${PORT}'|\" 
	/etc/default/prometheus-postgres-exporter && systemctl restart prometheus-postgres-exporter" 
