#!/bin/bash
# Run an exchange against an independent db created with db-cluster.sh

if [[ -z ${1} ]]; then
  echo "Usage: ./exch-cluster.sh <N>"
  echo ""
  echo "Creates an independent Exchange instance running against a db on port 5432+N"
  echo "Call after espec was run - only works with partitioning"
  exit 2
fi

source /root/scripts/helpers.sh

DB_PORT=$((5432 + ${1}))

systemctl stop taler-exchange-*

sed -i -e "s/exchange-secmod-rsa/exchange-secmod-rsa-${1}/g" \
       -e "s/exchange-secmod-cs/exchange-secmod-cs-${1}/g" \
       -e "s/exchange-secmod-eddsa/exchange-secmod-eddsa-${1}/g" \
       /etc/taler/conf.d/exchange-secmod.conf

echo "
[exchangedb-postgres]
CONFIG=postgresql://${DB_USER}:${DB_PASSWORD}@db.${DNS_ZONE}:${DB_PORT}/${DB_NAME}
" > /etc/taler/secrets/exchange-db.secret.conf

rm -rf /var/lib/taler/exchange-offline/*

MASTER_KEY=$(sudo -u taler-exchange-offline taler-exchange-offline setup)

sed -i -e "s/MASTER_PUBLIC_KEY.*/MASTER_PUBLIC_KEY = ${MASTER_KEY}/g" \
	/etc/taler/conf.d/exchange-business.conf

# Setup the shared key directory when we use a secondary node
if [[ ${NUM_EXCHANGES} != "1" ]]; then
  rm -rf /home/${G5K_USER}/taler/exchange-secmod-{cs,rsa,eddsa}-${1} || true
  mkdir -p /home/${G5K_USER}/taler/exchange-secmod-{cs,rsa,eddsa}-${1}
fi

systemctl restart taler-exchange-httpd@10000.service

wait_for_keys "${NODE_NAME}.${DNS_ZONE}:10000/management"

sleep 5

taler-exchange-offline download > sig-req.json
taler-exchange-offline sign < sig-req.json > sig-res.json
taler-exchange-offline enable-account "payto://x-taler-bank/bank.${DNS_ZONE}/exchange?receiver-name=exchange" > acct-res.json
taler-exchange-offline wire-fee $(date +%Y) x-taler-bank KUDOS:0 KUDOS:0 > fee-res.json
taler-exchange-offline upload < sig-res.json
taler-exchange-offline upload < acct-res.json
taler-exchange-offline upload < fee-res.json

source ~/scripts/exchange-wirewatch.sh init-start
source ~/scripts/exchange-aggregator.sh init-start
source ~/scripts/exchange-closer.sh init-start
source ~/scripts/exchange-transfer.sh init-start

let "START=1+10000"
let "END=$START+$((${NUM_EXCHANGE_PROCESSES}-1))-1"

for PORT in $(seq $START $END); do
  systemctl restart taler-exchange-httpd@"${PORT}".socket \
                    taler-exchange-httpd@"${PORT}".service
  sleep 0.05
done
