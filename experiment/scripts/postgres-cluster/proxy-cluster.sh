#!/bin/bash
# Setup an exchange proxy to serve only a single exchange

if [[ -z ${1} ]]; then
  echo "Usage: ./proxy-cluster.sh <N1...Nn>"
  echo ""
  echo "Setup an exchange proxy to serve only for the specified exchange instance"
  echo "(deletes the ones matching the arguments N1-Nn)"
  echo "Call after espec was run"
  echo "Deletes all exchange-N1...Nn entries in /etc/nginx/sites-enabled/exchange"
  exit 2
fi

for i in $@; do
  sed -i "/exchange-${i}.${DNS_ZONE}/d" /etc/nginx/sites-enabled/exchange
done

systemctl reload nginx
