#!/bin/bash

cd ~/taler/wallet-core

git checkout .

#   -e "/reserve found/,+43 d" \
sed -i \
    -e "s/checkReserve(http, benchConf.exchange, reserveKeyPair.pub)/checkReserve(http, benchConf.exchange, reserveKeyPair.pub, 30000)/g" \
    -e "/const refreshDenoms/,+14 d" \
    -e "s/\* 10/\* 8/g" \
    packages/taler-wallet-cli/src/bench2.ts

make -j $(nproc) install

