#!/bin/bash
echo "
Rebuild the taler binaries from source
The following optional variables can be set:

<LIBMICROHTTPD|GUNET|EXCHANGE|MERCHANT|WALLET>_COMMIT_SHA

If not set the corresponding repo will not be rebuilt.

Optionally, CFLAGS can be passed with:

<LIBMICROHTTD|GNUNET|EXCHANGE|MERCHANT>_CFLAGS
"

set -e
TALER_HOME=~/taler

# Prepare the repository
# $1: Git repo to clone
# $2: Commit to checkout to
function prepare_repo() {
  SRC_DIR="${TALER_HOME}/$(basename ${1%.*})"
  test -d "${SRC_DIR}" || git clone "${1}" "${SRC_DIR}"
  cd "${SRC_DIR}"
  git checkout master > /dev/null && \
    (git pull > /dev/null 2>&1 || true)
  git checkout "$2" > /dev/null && \
    (git pull > /dev/null 2>&1 || true)
}

# Build the binaries in the current directory with make
# (runs ./bootstrap & ./configure)
# $1: optional CFLAGS
function build() {
  echo "INFO running bootstrap and configure"
  ./bootstrap
  if [ -f contrib/gana.sh ]; then
    ./contrib/gana.sh
  fi
  CFLAGS="$1" ./configure --enable-logging=verbose --prefix=/usr || CFLAGS="$1" ./configure
  make -j $(nproc)
}

# Install from a git repo
# $1: Git repo to clone
# $2: Commit to checkout to
# $3: Optional CFLAGS for ./configure
function install_repo() {
  prepare_repo "$1" "$2" 
  build "$3"
  echo "INFO installing"
  make install
  ldconfig
}

if [ ! -d "${TALER_HOME}" ]; then
  mkdir "${TALER_HOME}"
fi

if [[ -n ${LIBMICROHTTPD_COMMIT_SHA} ]]; then
  echo "INFO installing libmicrohttpd"
  install_repo "https://git.gnunet.org/libmicrohttpd.git" \
               "${LIBMICROHTTPD_COMMIT_SHA:-master}" \
               "${LIBMICROHTTPD_CFLAGS}"
fi

if [[ -n ${GNUNET_COMMIT_SHA} ]]; then
  echo "INFO installing GNUnet"
  install_repo "https://git.gnunet.org/gnunet.git" \
               "${GNUNET_COMMIT_SHA:-master}" \
     	       "${GNUNET_CFLAGS}"
fi

if [[ -n ${EXCHANGE_COMMIT_SHA} ]]; then
  echo "INFO installing Taler Exchange"
  install_repo "https://git.taler.net/exchange.git" \
               "${EXCHANGE_COMMIT_SHA:-master}" \
               "${EXCHANGE_CFLAGS}"
fi

if [[ -n ${MERCHANT_COMMIT_SHA} ]]; then
  echo "INFO installing Taler Merchant"
  install_repo "https://git.taler.net/merchant.git" \
               "${MERCHANT_COMMIT_SHA:-master}" \
               "${MERCHANT_CFLAGS}"
fi

if [[ -n ${WALLET_COMMIT_SHA} ]]; then
  echo "INFO installing Taler Wallet"
  install_repo "https://git.taler.net/wallet-core.git" \
               "${WALLET_COMMIT_SHA:-master}" \
               ""
fi
