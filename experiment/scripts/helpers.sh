#/!bin/bash
# Useful functions which are neeeded across
# multiple scripts
#
# Usage: source ./helpers.sh

# Disable verbose output when loading .env so secret
# Variables are not in logs unecessarily
set +x
source ~/.env
set -x

# Set a dynamic domain name in our own dns
# IP address will be resolved on the node this function
# is executed on
# $1: The domain to be added
function set_ddn() {
  # wait for bind to be ready
  while ! nc -z ${DNS_IP} 53; do
    sleep 1
  done
  IP=$(ip -o route get to 8.8.8.8 | sed -n 's/.*src \([0-9.]\+\).*/\1/p')
  nsupdate -v << EOF
server ${DNS_IP}
zone ${DNS_ZONE}
update add ${1} 3600 A ${IP}
send
EOF
}

# Set the host (role) which the node executing this
# function is assigned to in the experiments
# This can later be used to setup log directories per role or in
# log entries - such as net-delay (ping.sh) for example
function set_host() {
  # Add it to all sources and export it to the current shell
  echo "TALER_HOST=${1}" >> /etc/environment
  echo "TALER_HOST=${1}" >> /root/.env
  export "TALER_HOST=${1}"
}

# Enable the service which logs round trip times between nodes
# $1: Destination to ping
# NOTE: currenly only one destination is supported
function enable_netdelay() {
  sed -i "s/<PING_DESTINATION>/${1}/g" \
	  /usr/lib/systemd/system/taler-netdelay.service
  systemctl daemon-reload
  systemctl restart taler-netdelay.timer
}

# Setup the log directiories in the Grid5000 shared home directory (NFS)
# And configure rsyslog to send logs to promtail
function setup_log() {
  HOST_LOG_DIR=${LOG_DIR}/${TALER_HOST}
  # || true is sill needed when e.g. wallets want to create the same 
  # directory at the same time
  test -d ${HOST_LOG_DIR} || mkdir ${HOST_LOG_DIR} || true 
  # Send all logs about taler to promtail on the monitoring node
  sed -i -e "s/<MONITOR_DOMAIN_HERE>/monitor.${DNS_ZONE}/g" \
	 -e "s/<PROMTAIL_LOG_PORT_HERE>/${PROMTAIL_LOG_PORT}/g" \
         -e "s|<LOG_DIR_HERE>|${HOST_LOG_DIR}|g" \
  	/etc/rsyslog.d/taler.conf
  # Enable log rotating for all logs in the host log dir on NFS
  sed -i "s|<LOG_DIR_HERE>|${HOST_LOG_DIR}|g" \
          /etc/logrotate.d/taler
}

# Enable the logbackup (NFS) - logrotation service
function enable_logrotate() {
  systemctl restart taler-logrotate.timer
}

# Wait for the database to be acessible by $DB_USER from remote
function wait_for_db() {
  until PGPASSWORD="${DB_PASSWORD}" psql \
        -h "db.${DNS_ZONE}" \
        -U "${DB_USER}" \
        -d "${DB_NAME}" \
	-p "${DB_PORT}" \
        -c '\q';
  do
    echo "Database not ready yet"
    # sometimes dns is not working correctly - try to fix with restart
    systemctl restart dnsmasq
    sleep 5
  done
}

# Wait until the exchange is ready to present key materials
# $1: Domain to request /keys from (exch.perf.taler</management>)
function wait_for_keys() {
  until wget http://${1}/keys \
        --spider \
        --timeout=5 \
        --tries=1 \
        --quiet;
  do
    echo "Exchange not ready yet"
    # sometimes dns is not working correctly - try to fix with restart
    systemctl restart dnsmasq
    sleep 5
  done
} 

# Nginx will log to our rsyslog directly an then from there it will be 
# redirected to promtail - there was an issue doing it directly that's
# why it is done this way
function setup_rsyslog_for_nginx() {
  sed -i -e '/module(load="imudp")/s/^#//g' \
         -e '/input(type="imudp" port="514")/s/^#//g' \
   	   /etc/rsyslog.conf
}

# Get the exchanges master public key via the primary exchanges API
function get_exchange_masterkey() {
  MASTER_KEY=$(
    curl -k -f \
       "${PRIMARY_EXCHANGE}:10000/keys" \
       | jq -r '.master_public_key'
  )
  echo "${MASTER_KEY}"
}


# Setup taler.conf for aggregator etc. with downloading the 
# master public key from the exchange API 
function setup_exchange_config_master_key_from_api() {

  setup_exchange_config_without_master_key "http://${EXCHANGE_GW_DOMAIN}/"

  wait_for_keys "${PRIMARY_EXCHANGE}:10000"

  MASTER_KEY=$(get_exchange_masterkey)
  sed -i -e "s/<MASTER_KEY_HERE>/${MASTER_KEY}/g" \
  	/etc/taler/conf.d/exchange-business.conf

}

# Setup taler.conf for any exchange-* process, does not configure
# the master key
# $1: Exchange base url
function setup_exchange_config_without_master_key() {

  AGGREGATOR_SHARD_SIZE=$(echo "2^(30-${NUM_AGGREGATOR_PROCESSES})" | bc)

  sed -i -e "s\<DB_URL_HERE>\postgresql://${DB_USER}:${DB_PASSWORD}@db.${DNS_ZONE}:${DB_PORT}/${DB_NAME}\g" \
         -e "s/<SHARD_USER_HERE>/${DB_USER}/g" \
	 -e "s/<SHARD_USER_PW_HERE>/${DB_PASSWORD}/g" \
  	/etc/taler/secrets/exchange-db.secret.conf

  sed -i -e "s/<EXCHANGE_CIPHER_HERE>/${EXCHANGE_CIPHER}/g" \
         -e "s/<RSA_KEY_SIZE_HERE>/${RSA_KEY_SIZE}/g" \
	 /etc/taler/conf.d/exchange-coins.conf

  sed -i "s/<BANK_HOST_HERE>/bank.${DNS_ZONE}/g" \
  	/etc/taler/secrets/exchange-accountcredentials.secret.conf

  sed -i -e "s/<BANK_HOST_HERE>/bank.${DNS_ZONE}/g" \
         -e "s|<BASE_URL_HERE>|${1}|g" \
         -e "s/<MAX_REQUESTS_HERE>/${EXCHANGE_MAX_REQUESTS:-8192}/g" \
         -e "s/<AGGREGATOR_SHARD_SIZE_HERE>/${AGGREGATOR_SHARD_SIZE}/g" \
	 -e "s/<SHARD_DOMAIN_HERE>/${DNS_ZONE}/g" \
  	/etc/taler/conf.d/exchange-business.conf

  sed -i "s/<LOOKAHEAD_SIGN_HERE>/${LOOKAHEAD_SIGN}/g" \
      /etc/taler/conf.d/exchange-secmod.conf

  if [[ ${NUM_EXCHANGES} != "1" ]]; then
    # Use a shared NFS key directory when we have multiple exchange-httpd servers
    sed -i "s|<SM_KEY_DIR_HERE>|/home/${G5K_USER}/taler|g" \
        /etc/taler/conf.d/exchange-secmod.conf
  else
    # Use the default path if we have only one exchange-httpd server
    sed -i 's/<SM_KEY_DIR_HERE>/${TALER_DATA_HOME}/g' \
        /etc/taler/conf.d/exchange-secmod.conf
  fi

}

# Restart (enable) the rsyslog to send the logs to the monitoring
# Node and to the shared log dir (NFS)
function restart_rsyslog() {
  # rsyslg fails to apply the taler rule if remote is not reachable
  while ! nc -z "monitor.${DNS_ZONE}" 1514;
  do
    echo "Waiting for promtail"
    # There are issues when dnsmasq tries to resolve before the service
    # is ready, when it gets restarted it works
    systemctl restart dnsmasq
    sleep 5
  done
  
  systemctl restart rsyslog
}

# Stop services which have the form name@number.service
# will stop the services starting from the biggest numbers
# $1: service base name, e.g. taler-exchange-httpd
# $2: amount of services to stop
function stop_numbered_services() {
  # Get all running processes of the service $1,
  # extract their number, limit by number of
  # to stop and stop those
  N=$(\
    systemctl status "${1}"@*.service | \
    grep -E "${1}@[0-9]+.service -" | \
    awk '{print $2}' | \
    sed 's/[^0-9]*//g' | \
    sort -n -r | \
    head -n $2 \
  )
  for i in ${N}; do
    systemctl stop ${1}@${i}.service
  done
}

# Get all hosts which are registered under $1
# Returns the host name - <HOST_NAME>.${DNS_ZONE}
# $1: a regex for a domain/host to search for
function get_hosts() {
  IFS=$'\n' read -r -d '' -a HOSTS < <(\
    dig -t AXFR "${DNS_ZONE}" "@${DNS_HOSTS}" \
    | grep ${1} | awk '{print $1}' | cut -d '.' -f 1 \
  )
  echo ${HOSTS[@]}
}

# Create a TLS certificate for $1
# $1: domain to create cert for
# $2: file name x to save x.key.pem and x.cert.pem
function create_cert() {
  openssl req -new -x509 \
              -newkey rsa:4096 \
	      -keyout ${2}.key.pem \
	      -out ${2}.cert.pem \
	      -sha256 -days 10 -nodes \
	      -subj "/C=CH/ST=Bern/L=Biel/O=TI/CN=${1}"
}
  

# Display a help message and exit
# $1: script name to display help for
# $2: info message about the script to display
# $3: option description to display
function taler_perf_help() {
  set +x
  echo "=================== Taler Performance Help ===================="
  echo "$2"
  echo "Usage: $1 OPTIONS"
  echo ""
  echo "OPTIONS"
  echo ""
  echo "$3"
  echo "==============================================================="
  exit 2
}
