#!/bin/bash
INFO_MSG="
Setup the Exchange Closer node
Start taler-exchange-closer
"
OPT_MSG="
init:
  Initialize the application(s) and start them
  uses NUM_CLOSER_PROCESSES

init-start:
  Same as init but do not configure taler.conf,
  just start the service
"

set -eux
source ~/scripts/helpers.sh

# Start N new exchange-aggregator processes
# $1: N - number of new aggregators to start
# NOTE: only for init purposes currently
function start_closers() {
  restart_rsyslog
  for i in $(seq ${1}); do
    systemctl restart taler-exchange-closer@"${i}".service
  done
}

case $1 in
  init)
    setup_exchange_config_master_key_from_api
    start_closers "$NUM_CLOSER_PROCESSES"
    ;;
  init-start)
    start_closers "$NUM_CLOSER_PROCESSES"
    ;;
  *)
    taler_perf_help $0 "$INFO_MSG" "$OPT_MSG"
    ;;
esac
