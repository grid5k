#!/bin/bash

# Script which creates a snapshot of a running Prometheus instance

# This will copy the snapshot to the configured LOG_DIR
# (setup.sh) hopefully persistent on the Grid5000 NFS

# Usage: ./data-backup.sh
# Used by taler-databackup.service

set -eu

if [[ $(ps -aux | grep "[data]-backup.sh" | wc -l) -eq 1 ]]; then
  echo "Previous backup still running, skipping"
  exit 0
fi

PROMETHEUS_DATA_DIR=/var/lib/prometheus/metrics2/snapshots

SNAPSHOT_COPY_PATH=${EXPERIMENT_DATA_DIR}/prometheus-tmp
SNAPSHOT_PATH=${EXPERIMENT_DATA_DIR}/prometheus

END_TIME=$(date +%s)

SNAPSHOT="${PROMETHEUS_DATA_DIR}/$(\
  curl \
    -X POST \
    http://localhost:8080/api/v1/admin/tsdb/snapshot \
  | jq -r '.data.name'
)"

# copy first because its nfs, when copied remove the previous one
cp -r "${SNAPSHOT}" "${SNAPSHOT_COPY_PATH}"
rm -rf "${SNAPSHOT_PATH}"
mv "${SNAPSHOT_COPY_PATH}" "${SNAPSHOT_PATH}"

# export timestamps for the recover script in g5k-repo/additional
echo "
EXPERIMENT_START=${START_TIME}
SNAPSHOT_TIME=${END_TIME}
" > "${EXPERIMENT_DATA_DIR}/times.env"

