#!/bin/bash
INFO_MSG="
Setup the Exchange Aggregator node
Start taler-exchange-aggregator 
"
OPT_MSG="
init:
  Initialize the application(s) and start them
  uses NUM_AGGREGATOR_PROCESSES

init-start:
  Same as init but do not configure taler.conf,
  just start the service
"

set -eux
source ~/scripts/helpers.sh

# Start N new exchange-aggregator processes
# $1: N - number of new aggregators to start
# NOTE: only for init purposes currently
function start_aggregators() {
  restart_rsyslog
  for i in $( seq $(echo "2^${1}" | bc) ); do
    systemctl restart taler-exchange-aggregator@"${i}".service
  done
}

case $1 in
  init)
    setup_exchange_config_master_key_from_api
    start_aggregators "$NUM_AGGREGATOR_PROCESSES"
    ;;
  init-start)
    start_aggregators "$NUM_AGGREGATOR_PROCESSES"
    ;;
  *)
    taler_perf_help $0 "$INFO_MSG" "$OPT_MSG"
    ;;
esac
