#!/bin/bash
set -eux
source ~/scripts/helpers.sh

INFO_MSG="
Setup the merchant node
(Start taler-merchant-httpd)
"
OPT_MSG="
init:
  Configure and start the merchant together with its database
"

# Prevent change directory errors
cd /tmp

# Configurre the merchants files in /etc/taler
function configure_merchant() {
  wait_for_keys "${EXCHANGE_GW_DOMAIN}"

  MASTER_KEY=$(
    curl -k -f \
       "${EXCHANGE_GW_DOMAIN}/keys" \
       | jq -r '.master_public_key'
  )

  sed -i -e "s\<EXCHANGE_URL_HERE>\http://${EXCHANGE_GW_DOMAIN}/\g" \
         -e "s/<EXCHANGE_MASTER_KEY_HERE>/${MASTER_KEY}/g" \
          /etc/taler/conf.d/merchant.conf
}

# Setup the merchants db on the same host
function configure_db() {
  # Allow the remote hosts (monitor) access with the postgres user
  echo "
  host all postgres 172.16.0.0/12 trust
  " >> /etc/postgresql/${POSTGRES_VERSION}/main/pg_hba.conf

  # Listen on all interfaces so the monitors db exporter can reach the db
  echo "
  listen_addresses='*'
  shared_preload_libraries='pg_stat_statements,auto_explain'
  " >> /etc/postgresql/${POSTGRES_VERSION}/main/postgresql.conf
}

# Start postgres an initialize the merchant's database
function init_db() {
  systemctl restart postgresql

  su postgres << EOF
  psql postgres -tAc "SELECT 1 FROM pg_roles WHERE rolname='taler-merchant-httpd'" | \
    grep -q 1 || \
    createuser taler-merchant-httpd
  psql -tAc "SELECT 1 FROM pg_database WHERE datname='taler-merchant'" | \
    grep -q 1 || \
    createdb -O taler-merchant-httpd taler-merchant
  psql
  CREATE EXTENSION pg_stat_statements;
EOF

  sudo -u taler-merchant-httpd taler-merchant-dbinit
}

# Create a new merchant instance
# $1: Instance name
function create_instance() {
  curl -X POST \
       -H "Authorization: Bearer secret-token:${TALER_MERCHANT_TOKEN}" \
       -H "Content-Type: application/json" \
       -d "{
             \"accounts\": [{\"payto_uri\":\"payto://x-taler-bank/bank.${DNS_ZONE}/${1}?receiver-name=${1}\"}],
	     \"id\": \"${1}\",
	     \"name\": \"${1}\",
	     \"address\": {\"country\": \"CH\"},
	     \"auth\": {\"method\": \"token\", \"token\": \"secret-token:${TALER_MERCHANT_TOKEN}\"},
	     \"jurisdiction\": {\"country\": \"CH\"},
	     \"default_max_wire_fee\": \"KUDOS:1\",
	     \"default_wire_fee_amortization\": 10,
	     \"default_max_deposit_fee\": \"KUDOS:1\",
	     \"default_wire_transfer_delay\": {\"d_ms\": 1209600000},
	     \"default_pay_delay\": {\"d_ms\": 1209600000}
	   }" \
        http://localhost/management/instances
}

# Initialize the merchant
function init_merchant() {
  create_cert "${NODE_NAME}.${DNS_ZONE}" "/etc/ssl/merchant"
  setup_rsyslog_for_nginx
  restart_rsyslog
  configure_merchant
  configure_db
  init_db
  mkdir /var/lib/taler || true
  ln -sf /etc/nginx/sites-available/merchant /etc/nginx/sites-enabled/merchant
  systemctl restart taler-merchant-httpd \
                    nginx
  sleep 5
  create_instance "default"
  create_instance ${NODE_NAME}
}

case $1 in
  init)
    init_merchant
    ;;
  *)
    taler_perf_help $0 "$INFO_MSG" "$OPT_MSG"
    ;;
esac

exit 0
