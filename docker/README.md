# Taler Grid'5000 Build Image

This docker image can be used to build the Grid'5000 environment for the taler performance experiments

## Build

Build this image with `docker build . -t taler:build`
Or alternatively with `docker-compose up --build` **NOTE** this will also run the image, read below

## Run the build

Running the image will build GNUnet, Taler and the Grid'5000 environment from the specified commits. 

The environment will then be uploaded to the specified nodes Grid5000 public directory using the certificate provided.

### docker

```bash
docker run -it --rm \
           --device=/dev/kvm:/dev/kvm \
           --device=/dev/fuse:/dev/fuse \
           --device=/dev/net/tun:/dev/net/tun \
           -p 5900:5900 \
           --cap-add NET_ADMIN \
           --cap-add SYS_ADMIN \
           --security-opt apparmor:unconfined \
           -e GRID5K_USER=<user> \
           -e GRID5K_CERT_PASSWD=<cert_passwd> \
           -v <cert_path>:/root/cert.pem \
           taler:build <ARGUMENTS>
```

**NOTE** About the port 5900: this one can be used for vncviewer to see whats happening inside the image while 
kameleon is running. Run `vncviewver :0`.

#### Manual Build / Debugging

To get an interactive shell into the docker container override the entrypoint by adding the following argument
before the last line in the command above:

```bash
--entrypoint=/bin/bash
```

### docker-compose

Assuming an env file `.env` with the following contents:

```bash
GRID5K_USER=<user>
GRID5K_CERT=<cert_path>
```

the build can be started with:

```bash
ARGUMENTS=<ARGUMENTS> GRID5K_CERT_PASSWD=<cert_passwd> docker-compose up --build
```

Or place the password (also arguments) in .env too but make sure its protected.

#### Manual Build

You can also use the docker compose to get an interactive shell into the image:

```bash
docker-compose run --entrypoint /bin/bash taler-build
```

### Notes

#### Environment Variables

All variables listed below can be passed to the container with either -e or by adding them to the docker-compose file.

**GRID5K_USER**: the user which `GRID5K_CERT` belongs to
**GRID5K_CERT**: the certificate which is used to login to the Grid5000 nodes (docker-compose only)
**GRID5K_CERT_PASSWD**: the password to decrypt `GRID5K_CERT`
**GRID5K_DEST**: comma separated list of where to copy the image to in the grid (default: lille,lyon)
**ARGUMENTS**: args to pass to entrypoint, one of 
  -r|--rebuild (rebuild the image)
  -n|--no-copy (do not copy the generated image to Grid5000 - make sure output volume is mounted - see below)
  --centos     (build the `centos8` image instead of the default `debian11`)
 As per default, running the docker command again will not clean or rebuild the image

##### Additional

**GNUNET_COMMIT_SHA**: Which commit to use of gnunet 
**EXCHANGE_COMMIT_SHA**: Which commit to use of taler-exchange 
**MERCHANT_COMMIT_SHA**: Which commit to use of taler-merchant 
**WALLET_COMMIT_SHA**: Which commit to use of wallet-core 
**GRID5K_COMMIT_SHA**: Which commit to use of this repo 
**LIBMICROHTTPD_CFLAGS**: CFLAGS to pass to ./configure when building libmicrohttpd
**GNUNET_CFLAGS**: CFLAGS to pass to ./configure when building gnunet
**EXCHANGE_CFLAGS**: CFLAGS to pass to ./configure when building taler-exchange
**MERCHANT_CFLAGS**: CFLAGS to pass to ./configure when building taler-merchant

These are listed explicitely in the docker-compose.yml and not via environments.
All of them default to master if they are not present in the compose or via environment.

#### VNC

What happens during the build can be inspected via the logs from docker. 
However, kameleon uses qemu which can be inspected with `vncviewer` in certain steps and when port 5900 is mapped:

```bash
apt install -y tigervnc-viewer
```

```bash
vncviewer :0
```

#### Output Volume

The image will be published to Grid'5000's public directory on a specified node.
Additionally the generated image can also be mounted to the host by passing `-v <some_path>:/root/output` do `docker run`
or with docker-compose:

```yaml
volumes:
  - <some_path>:/root/output
```

in `docker-compose.yaml`.
