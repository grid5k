#!/bin/bash
# Docker Entrypoint script
# Automatically builds the Taler performance 
# environment image for Grid5000

set -e

IMAGE=debian11

while [[ $# -gt 0 ]]; do
  case "$1" in 
    -r|--rebuild)
      REBUILD=true
      echo "INFO will rebuild image"
      shift
      ;;
    -n|--no-copy)
      COPY=false
      echo "INFO will not copy image to Grid5000"
      shift
      ;;
    --centos)
      IMAGE="centos8"
      shift
      ;;
    *)
      shift
      ;;
  esac
done

# Run a command and fail when there was an error
# $1: Command to run
# $2: Log file to write to (default /dev/null)
function run_command() {
  if ! $(eval ${1} > "${2:-/dev/null}" 2>&1) ; then
    echo "ERROR cmd ${1} failed"
    cat "${2}" || true
    exit 1
  fi
}

# Prepare a git repo 
# $1: Git repo name
# $2: Git commit to checkout to
function prepare() {
  cd "${TALER_HOME}/$1"
  git checkout master > /dev/null && (git pull > /dev/null 2>&1 || true)
  git checkout "$2" > /dev/null && (git pull > /dev/null 2>&1 || true)
}

if [ "$COPY" != false ];
then
  # Add the certificate to the agent 
  # no more passwords are needed when it succeeds
  eval $(ssh-agent)
  
  if [[ -f "/root/cert.pem" ]]; then

    if [[ "${GRID5K_CERT_PASSWD}" != "" ]]; then
      /usr/bin/expect -c "
      spawn ssh-add /root/cert.pem;
      expect {
        \"Enter passphrase for /root/cert.pem: \" {
          exp_send ${GRID5K_CERT_PASSWD}\n;
	  exp_continue
        }
        \"Identity added: /root/cert.pem (/root/cert.pem)\" {
	  exp_continue
        }
        eof {}
      }" > /tmp/expect.log
    else 
      ssh-add /root/cert.pem
    fi

  fi

  if ! ssh-add -L > /dev/null ; then
    echo "ERROR could not add certificate"
    cat /tmp/expect.log
    echo ""
    exit 1
  else
    echo "INFO added certificate"
  fi
fi

echo "INFO preparing Grid5000 repository"
prepare grid5k "${GRID5K_COMMIT_SHA}"

# Test if the image specification is correct
run_command \
    "kameleon dryrun -g g5k_user:${G5K_USER} ${TALER_HOME}/grid5k/image/${IMAGE}/taler-${IMAGE}.yaml" \
    "dryrun.log"

cd ${TALER_HOME}/grid5k/image/${IMAGE}

if [ ! -f "build/taler-${IMAGE}/taler-${IMAGE}.tar.zst" ] || \
   [ "$REBUILD" = true ];
then
  rm -rf /tmp/taler-${IMAGE} ${TALER_HOME}/taler-${IMAGE} || true
  echo "INFO building image"
  if [[ "$IMAGE" == "debian11" ]]; then
    yes r | kameleon build \
            -b /tmp taler-${IMAGE}.yaml \
            -g gnunet_commit_sha:${GNUNET_COMMIT_SHA:-master} \
               exchange_commit_sha:${EXCHANGE_COMMIT_SHA:-master} \
               merchant_commit_sha:${MERCHANT_COMMIT_SHA:-master} \
               wallet_commit_sha:${WALLET_COMMIT_SHA:-master} \
               grid5k_commit_sha:${GRID5K_COMMIT_SHA:-master} \
               libmicrohttpd_cflags:"${LIBMICROHTTPD_CFLAGS:--O2}" \
               gnunet_cflags:"${GNUNET_CFLAGS:--O2}" \
               exchange_cflags:"${EXCHANGE_CFLAGS:--O2}" \
               merchant_cflags:"${MERCHANT_CFLAGS:--O2}" \
               g5k_user:${GRID5K_USER} \
          | tee build.log
  else
    yes r | kameleon build \
            -b /tmp taler-${IMAGE}.yaml \
            -g g5k_user:${GRID5K_USER} \
               grid5k_commit_sha:${GRID5K_COMMIT_SHA:-master} \
          | tee build.log
  fi
  mv /tmp/taler-${IMAGE} ${TALER_HOME}
fi

cd ${TALER_HOME}/taler-${IMAGE}

if [ -f "/root/cert.pem" ] && [ "$COPY" != false ]; then

  mv taler-${IMAGE}.dsc taler-${IMAGE}.dsc.bak
  
  IFS=, read -ra G5K_HOSTS <<< "${GRID5K_DEST}" 

  for G5K_HOST in "${G5K_HOSTS[@]}"; do
    echo "Copying image to ${G5K_HOST}"

    sed "/g5k_tar_path/s/lyon/${G5K_HOST}/g" \
        taler-${IMAGE}.dsc.bak > taler-${IMAGE}.dsc

    scp -o StrictHostKeyChecking=no taler-${IMAGE}.tar.zst taler-${IMAGE}.dsc \
        "${GRID5K_USER}@access.grid5000.fr:${G5K_HOST}"/public/
  done 

fi

if [ -d "/root/output" ]; then
  echo "INFO copying image to output (mounted volume)" 
  mv taler-${IMAGE}.dsc.bak taler-${IMAGE}.dsc || true
  cp taler-${IMAGE}.tar.zst taler-${IMAGE}.dsc /root/output/ 
fi
