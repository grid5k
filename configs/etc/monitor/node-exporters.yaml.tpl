  - job_name: 'nodes'
    static_configs:
    - labels:
        component: 'dns'
      targets:
      - 'ns1.${DNS_ZONE}:9100'
    - labels:
        component: 'database'
      targets: 
      - 'db.${DNS_ZONE}:9100'
    - labels:
        component: 'bank'
      targets:
      - 'bank.${DNS_ZONE}:9100'
    - labels:
        component: 'exchange'
      targets:
      # <EXCHANGE_NODES_HERE>
    - labels:
        component: 'exchange-aggregator'
      targets:
      - 'aggregator.${DNS_ZONE}:9100'
    - labels:
        component: 'exchange-closer'
      targets:
      - 'closer.${DNS_ZONE}:9100'
    - labels:
        component: 'exchange-transfer'
      targets:
      - 'transfer.${DNS_ZONE}:9100'
    - labels:
        component: 'exchange-wirewatch'
      targets:
      - 'wirewatch.${DNS_ZONE}:9100'
    - labels:
        component: 'proxy'
      targets:
      # <PROXY_NODES_HERE>
    - labels:
        component: 'wallet'
      targets:
      # <WALLET_NODES_HERE>
    - labels:
        component: 'monitor'
      targets:
      - 'monitor.${DNS_ZONE}:9100'
    - labels:
        component: 'merchant'
      targets:
      # <MERCHANT_NODES_HERE>
    - labels:
        component: 'shard'
      targets:
      # <SHARD_NODES_HERE>
    - labels:
        component: 'auditor'
      targets:
      - 'auditor.perf.taler:9100'
